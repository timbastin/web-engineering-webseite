import React from "react";
import Navigator from "./container/Navigator";
import { hot } from "react-hot-loader/root";

const App: React.FunctionComponent = () => {
    return <Navigator />;
};

export default hot(App);
