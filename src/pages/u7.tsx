import React from "react";
import Page from "../components/Page";
import Exercise from "../components/Exercise";
import ExerciseGroup from "../components/ExerciseGroup";
import Sidebar from "../components/Sidebar";
import Code from "../components/Code";

const links = [
    {
        anchor: "7.1",
        title: "Performanzmessungen von DOM-Operationen",
    },
    {
        anchor: "7.2",
        title: "Rednerliste mit Zeitmessung",
    },
    {
        anchor: "7.3",
        title: "TopSort als WebApp",
    },
];
const U7 = () => {
    return (
        <Page title={"Übung 7 - DOM"}>
            <div className={"container"}>
                <div className={"row"}>
                    <div className={"col-md-3"}>
                        <Sidebar links={links} />
                    </div>
                    <div className={"col-md-9"}>
                        <div>
                            <ExerciseGroup task={"Performanzmessungen von DOM-Operationen"} taskNumber={"7.1"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Implementieren Sie Performanzmessungen zum Vergleich von innerHTML, innerText, textContent und outerHTML selbstständig in JavaScript durch Nutzung der DOM API. Geben Sie die Messergebnisse als Tabelle aus. Verwenden Sie die eingebauten Zeitmess-Funktionen performance.now (), siehe auch When-milliseconds-are-not-enough-performance-now. Suchen Sie eine möglichst kurze und elegante Lösung.\n" +
                                            "\n" +
                                            "Dabei ist zu beachten, dass Browser, um potenzielle Sicherheitsbedrohungen wie Spectre oder Meltdown zu minimieren, den zurückgegebenen Wert normalerweise um einen bestimmten Betrag runden. Dies führt zu einer gewissen Ungenauigkeit. Beispielsweise rundet Firefox die zurückgegebene Zeit in Schritten von 1 Millisekunde. Diese Zwangsrundung kann man jedoch wiederum abschalten mittels Firefox setting privacy.reduceTimerPrecision, siehe How to get microsecond timings in JavaScript since Spectre and Meltdown."
                                        }>
                                        <Code language={"html"}>{`<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Performanzmessungen</title>
</head>
<body id="body">
    <div id="root">
        <div id="child">Test Text</div>
    </div>
    <table id="table">
        <thead>
            <tr>
                <td>Methode</td>
                <td>Zeit</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>innerHTML</td>
                <td id="innerHTML"></td>
            </tr>
            <tr>
                <td>innerText</td>
                <td id="innerText"></td>
            </tr>
            <tr>
                <td>textContent</td>
                <td id="textContent"></td>
            </tr><tr>
                <td>outerHTML</td>
                <td id="outerHTML"></td>
            </tr>

        </tbody>
    </table>
    <script>
        const reset = () => {
            const root = document.getElementById("root");
            root.innerHTML = '<div id="child">Test Text</div>';
        }
        const results = {
            "textContent": 0,
            "innerHTML": 0,
            "innerText": 0,
            "outerHTML": 0
        }
        const child = document.getElementById("root").childNodes[0]
        Object.keys(results).forEach(method => {
            const timeNow = performance.now();

            // for (let i = 0; i < 1_000_000; i++) {
                child[method] = '<div>Das ist ein Test</div>'
            // }
            results[method] = performance.now() - timeNow;

            document.getElementById(method).innerText = results[method];
            reset();
        })
    </script>
</body>
</html>
`}</Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div className={"mt-5"}>
                            <ExerciseGroup task={"Rednerliste mit Zeitmessung"} taskNumber={"7.2"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            'Implementieren Sie die interaktive Anwendung "Rednerliste mit Zeitmessung" selbstständig in JavaScript durch Nutzung der DOM API und der Timer-Funktionen. Neue Redner sollen auf Knopfdruck hinzugefügt werden können. Deren Uhr wird dann sofort automatisch gestartet und alle anderen Uhren angehalten. Bei jedem Redner soll die individuelle, gemessene Redezeit sekundengenau angezeigt werden. Für jeden Redner soll es einen eigenen Start-/Stopp-Button geben. Es soll immer nur eine Uhr laufen. Angezeigt werden sollen die bisherigen Summenzeiten aller Redebeiträge der betreffenden Person. Suchen Sie eine möglichst kurze und elegante Lösung. Achten Sie gleichzeitig auf gute Usability: z.B. wenn die Eingabe mit einem Return beendet wird, soll der Button-Click nicht mehr erforderlich sein, usw.'
                                        }>
                                        <Code language={"html"}>
                                            {`<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Performanzmessungen</title>
</head>
<body id="body">
    <h1>Rednerliste</h1>
    <form id="form">
        <label for="input">
            Neuer Redner:
        </label>
        <input type="text" id="input" />
        <button>Hinzufügen</button>
    </form>
    <ul id="list">

    </ul>
    <script>
      let runningClock = null;
      let clockInterval = null;
      const clock = {};
      
      function formatClock(clock) {
          const hours = Math.floor(clock / 3600);
          const remainder = clock % 3600;
          const minutes = Math.floor(remainder / 60);
          const seconds = remainder % 60;
          return hours.toLocaleString("de", {minimumIntegerDigits: 2}) + ":" + minutes.toLocaleString("de", {minimumIntegerDigits: 2}) + ":" + seconds.toLocaleString("de", {minimumIntegerDigits: 2})
      }
      function updateClock(whichClock) {
          const el = document.getElementById(whichClock).getElementsByClassName("clock")[0];
          if (el) {
              el.innerText = formatClock(clock[whichClock]);
          }
      }
      
      function runClock(whichClock) {
          const button = document.getElementById(whichClock).getElementsByTagName("button")[0];
          button.innerText = "Stopp!";
          runningClock = whichClock;
          clockInterval = setInterval(() => {
              clock[whichClock] += 1;
              updateClock(whichClock)
          }, 1000)
      }
      
      function buttonHandler(whichClock) {
          const runningClock_ = runningClock;
          stopAllClocks();
          if (runningClock_ !== whichClock)
              runClock(whichClock);
      }
      
      function stopAllClocks() {
          runningClock = null;
          clearInterval(clockInterval);
          const elements = document.getElementById("list").getElementsByTagName("button");
          [...elements].forEach(el => {
              el.innerText = "Start!";
          })
      }
      
      function addListElement(name) {
          const list = document.getElementById("list");
      
          const li = document.createElement("li");
          li.id = name;
          li.innerHTML = "<span>" + name +"</span> <span class='clock'>00:00:00</span><button>Stop!</button>"
          list.append(li);
          document.getElementById(name).getElementsByTagName("button")[0].addEventListener("click", () => buttonHandler(name));
      }
      
      function handleSubmit(event) {
          event.preventDefault();
          const input = document.getElementById("input")
          clock[input.value] = 0;
          addListElement(input.value);
          stopAllClocks();
          runClock(input.value)
      }
      
      document.getElementById("form").addEventListener("submit", handleSubmit)
  </script>
</body>
</html>
`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div className={"mt-5"}>
                            <ExerciseGroup task={"TopSort als WebApp"} taskNumber={"7.3"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie eine Web-Oberfläche, in der man beliebige Beziehungen (Vorrang-Relationen) eingeben kann, für die dann die topologische Sortierung per Knopfdruck auf der Webseite ausgegeben wird.\n" +
                                            "\n" +
                                            "Für die Eingabe können Sie HTML5-Eingabefelder oder contentEditable verwenden."
                                        }>
                                        <Code language={"html"}>
                                            {`<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Topsort</title>
</head>
<body id="body">
    <h1>Topsort</h1>
    <form id="form">
        <div>
            <label for="input">
                Abhängigkeit
            </label>
            <input type="text" id="input" />
        </div>
        <div>
            <label for="input1">
                Aufgabe
            </label>
            <input type="text" id="input1" />
        </div>
        <button>Hinzufügen</button>
    </form>
    <div id="rel"></div>
    <button id="calc">Berechnen</button>

    <div id="result">
    </div>
    <script>
      class Vorrang {
        arr;
        solution;
        constructor(arr) {
            this.arr = arr;
            this.calculate();
        }
        calculate() {
            this.solution = this.topsort(this.arr);
        }
        hasNoDependency(el, arr) {
            return !arr.some(deps => deps.length > 1 && deps[1] === el);
        }
        topsort(arr) {
            const sort = [];
            while (arr.length > 0) {
                arr.forEach(deps => {
                    if (this.hasNoDependency(deps[0], arr)) {
                        sort.push(deps[0]);
                        // remove it from arr.
                        arr = arr.map(d => d.filter(task => task !== deps[0]));
                        arr = arr.filter(d => d.length > 0);
                    }
                });
            }
            return sort;
        }
      }
    
      const arr = [];
      
      document.getElementById("form").addEventListener("submit", e => {
          e.preventDefault();
          const dep = document.getElementById("input").value;
          const task = document.getElementById("input1").value;
      
          arr.push([dep, task]);
          document.getElementById("rel").innerText = JSON.stringify(arr);
          dep.value = "";
          task.value = "";
      })
      
      document.getElementById("calc").addEventListener("click", e => {
          e.preventDefault();
          const vorrang = new Vorrang(arr);
          document.getElementById("result").innerText = vorrang.solution;
      })
    </script>
</body>
</html>
`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                    </div>
                </div>
            </div>
        </Page>
    );
};

export default U7;
