import React from "react";
import Page from "../components/Page";
import Code from "../components/Code";

const Home = () => {
    return (
        <Page title={"Startseite"}>
            <div className={"container"}>
                <div className={"row"}>
                    <div className={"col-md-12"}>
                        <h1>Einführung in Web Engineering (WE)</h1>
                        <p>
                            Diese Webseite stellt die Hausarbeit des Kurses: &quot;Einführung in Web Engineering
                            (WE)&quot; der Hochschule Bonn Rhein-Sieg im Wintersemester 20/21 dar.
                        </p>
                        <p>
                            Der Source-Code des Projektes kann in einem öffentlichen{" "}
                            <a
                                rel={"noreferrer"}
                                target="_blank"
                                href={"https://gitlab.com/timbastin/web-engineering-webseite"}>
                                Gitlab-Repository
                            </a>
                            &nbsp; gefunden werden.
                        </p>
                        <h2>Aufbau</h2>
                        <p>Die Webseite ist in folgende Hauptpunkte strukturiert:</p>
                        <ol>
                            <li>Startseite</li>
                            <li>Übungen</li>
                            <li>Impressum</li>
                            <li>Vue-App</li>
                        </ol>
                        <p>
                            Des weiteren beinhaltet der &quot;Übungen&quot; Navigationspunkt weitere Unterseiten, welche
                            die einzelnen Übungen präsentieren. Das Rendern von HTML Code live im Browser geschieht
                            mittels Iframes.
                        </p>
                        <p>
                            Ebenfalls sind auf den Unterseiten Anchor-Links zu finden, welche eine schnelle Navigation
                            zu Übungsabschnitten möglich machen.
                        </p>
                        <h2>Technische Details</h2>
                        <p>
                            Diese Website wurde vollständig mit TypeScript geschrieben aufgrund der statischen
                            Typisierung. Das führt zu weniger Laufzeitfehlern und einer besseren Dokumentation des Codes
                            durch die explizite Beschreibung von spezifischen Typen.
                        </p>
                        <p>
                            Es handlet sich um eine komponentenbasierte Entwicklung auf Basis von ReactJS und JSX.
                            Gleichzeitig bedient sie sich dem <i>scss</i> preprocessor, um die Arbeit mit CSS zu
                            erleichtern und effizienter zu gestalten.
                            <br />
                            Für das Bundling wird Webpack eingesetzt, der auch dafür zuständig ist, Vue.js und PHP Code
                            in den <i>dist</i> Ordner zu kopieren, was den Deploy der Website vereinfacht. Babel
                            hingegen transpiliert den TypeScript Code zu JavaScript.
                        </p>
                        <p>
                            ESLINT und prettier sorgen dafür, dass jegliche definierten Clean-Code Regeln eingehalten
                            werden.
                        </p>
                        <b>Webpack.config.js</b>
                        <Code noIframe={true} language={"javascript"}>{`var path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  mode: "development",
  entry: "./src/index.ts",
  output: {
    filename: "bundle.js",
    path: __dirname + "/dist",
    publicPath: "/static/",
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".json"],
  },
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    hot: true,
    port: 9000,
  },
  devtool: "source-map",
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
          // relative path is from src
        { from: "./static/" },
          // copy the vue-js app to the dist directory.
          // we depend on the installation and building of it.
          // good that we have a ci/cd
        { from: "vue-app/dist", to: "vue-app" },
        { from: "php", to: "php" },
      ],
    }),
  ],
  module: {
    rules: [
      {
        test: /\.scss$/,
        include: path.resolve(__dirname, "src"),
        use: ["style-loader", "css-loader", "sass-loader"],
      },
      {
        test: /\.less$/,
        include: path.resolve(__dirname, "src"),
        use: [
          { loader: "style-loader" },
          { loader: "css-loader" },
          { loader: "less-loader", options: { lessOptions: { javascriptEnabled: true } } },
        ],
      },
      { test: /\.tsx?$/, include: path.resolve(__dirname, "src"), loader: "babel-loader" },
      { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },
    ],
  },
};`}</Code>
                        <p>
                            <a target={"_blank"} rel={"noreferrer"} href={"https://ant.design/"}>
                                ant-design
                            </a>
                            &nbsp;verwendet. Diese Bibliothek erwies sich insbesondere bei der Erstellung der Navigation
                            als nützlich.
                        </p>
                        <p>
                            Das grobe Layout-ing der Webseite geschieht primär mit dem{" "}
                            <a href={"https://getbootstrap.com/"} rel={"noreferrer"}>
                                Bootrap Framework
                            </a>
                        </p>
                        <h2>Die Wichtigsten React-Komponenten</h2>
                        <ol>
                            <li>AnchorLink.tsx: ermöglicht Smooth-Scrolling</li>
                            <li>Code.tsx: Syntax-Highlighting + Iframe-Rendering von HTML-Content</li>
                            <li>ExerciseGroup.tsx: Layouting einzelner Abschnitte</li>
                            <li>Exercise.tsx: Layouting einzelner Übungen</li>
                            <li>Nav.tsx: Navigation</li>
                            <li>Page.tsx: Layouting ganzer Seiten</li>
                            <li>Sidebar.tsx</li>
                            <li>Header.tsx</li>
                            <li>Footer.tsx</li>
                        </ol>
                        <h2>Bibliotheken</h2>
                        <ol>
                            <li>ant-design als Komponentenbibliothek</li>
                            <li>bootstrap</li>
                            <li>react-router-dom</li>
                            <li>react-dom</li>
                            <li>react-syntax-highlighter</li>
                            <li>lodash</li>
                        </ol>
                        <h2>Deployment (CI / CD)</h2>
                        <p>
                            Für das automatisierte Deployment wird die Gitlab Pipeline benutzt mittels .gitlab-ci.yml
                            Datei. Insgesamt wird rsync für das Deployment über SSH eingesetzt, und das Passwort in
                            einer geschützen Umgebungsvariablen gespeichert. Ausschließlich der <i>dist</i> Ordner wird
                            auf den Produktionsserver verschoben.
                        </p>
                        <b>.gitlab-ci.yml</b>
                        <Code noIframe={true} language={"yaml"}>{`image: node:14

stages:
  - build
  - deploy

build:
  stage: build
  before_script:
    - npm i
  script:
    - cd vue-app
    - npm i
    - npm run build
    - cd ..
    - npm run webpack:prod
  artifacts:
    paths:
      - dist/
  cache:
    paths:
      - node_modules/

deploy:
  stage: deploy
  image: uilicious/alpine-openssh-rsync
  script:
    - sshpass -e rsync --progress -avz -e "ssh -o StrictHostKeyChecking=no" ./dist/* tbasti2s@www2.inf.h-brs.de:~/public_html/`}</Code>
                    </div>
                </div>
            </div>
        </Page>
    );
};

export default Home;
