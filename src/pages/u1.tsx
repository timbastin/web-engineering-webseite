import React from "react";
import Page from "../components/Page";
import Exercise from "../components/Exercise";
import ExerciseGroup from "../components/ExerciseGroup";
import Sidebar from "../components/Sidebar";
import Code from "../components/Code";

const links = [
    {
        anchor: "1.1",
        title: "Fachliche Argumentation über Erfolgsprinzipien des WWW",
    },
    {
        anchor: "1.2",
        title: "HTTP",
    },
    {
        anchor: "1.3",
        title: "HTML-Literatur lesen und Fragen beantworten",
    },
    { anchor: "1.4", title: "HTML Wireframe" },
];
const U1 = () => {
    return (
        <Page title={"Übung 1 - Einführung"}>
            <div className={"container"}>
                <div className={"row"}>
                    <div className={"col-md-3"}>
                        <Sidebar links={links} />
                    </div>
                    <div className={"col-md-9"}>
                        <div>
                            <ExerciseGroup
                                task={"Fachliche Argumentation über Erfolgsprinzipien des WWW"}
                                taskNumber={"1.1"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Mit welchen fachlichen Argumenten wurde das WWW-Proposal von TBL abgelehnt?"
                                        }>
                                        Das Proposal wurde abgelehnt, da es broken links geben könnte. Es herrschte die
                                        Meinung, dass man schon &quot;weiter&quot; sei als Tim Berners-Lee
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Was sind die fachlichen Argumente, warum das WWW dennoch ein Erfolg wurde?"
                                        }>
                                        Das WWW wurde dennoch ein Erfolg, da es sich inbesondere durch die
                                        Dezentralität, Evolvierbarkeit und Offenheit auszeichnet. Die benutzen
                                        Technologien können sich so unabhängig voneinander weiter entwickeln.
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Was wäre der Preis für die garantierte Verhinderung von “broken links”?"
                                        }>
                                        Der Preis für referentielle Integrität wäre eine Zentrale Speicherung der Daten.
                                        Nur so kann gewährleistet werden, dass die referenzierte Quelle existiert.
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div className={"mt-5"}>
                            <ExerciseGroup task={"HTTP"} taskNumber={"1.2"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={"Sie bekommen im Browser den HTTP Status Code 200. Was bedeutet das?"}>
                                        Alles ist OK.
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Sie bekommen im Browser den HTTP Status Code 301. Was hat das zu bedeuten?"
                                        }>
                                        Permanent Redirect - Die angefragte Resource ist nun unter einer anderen URL zu
                                        finden. Die neue URL wird in der Antwort mitgeschickt. Das standard Browser
                                        verhalten ist hier ein &quot;Follow&quot;, sprich die neue URL anzufragen.
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Sie bekommen im Browser den HTTP Status Code 400. Was hat das zu bedeuten? Was können Sie dagegen tun?"
                                        }>
                                        400 bedeutet Bad Request. Die Anfrage konnte vom Server nicht verarbeitet
                                        werden. In dem Gedankenspiel liegt der Fehler auf meiner Seite (Client).
                                        Vielleicht hilft es, sich noch einmal die Dokumentation anzuschauen des zu
                                        verwendenen Servers.
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Sie bekommen im Browser den HTTP Status Code 403. Was hat das zu bedeuten? Was können Sie dagegen tun?"
                                        }>
                                        Forbidden. Mir fehlt die Berechtigung die Seite aufzurufen oder die Resource zu
                                        bearbeiten. Als Hilfe könnte ich einen Administrator informieren, sollte ich
                                        sicher sein, dass mir diese Berechtigung eigentlich zusteht.
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "In einer Webanwendung benötigen Sie eine OPTIONS-Anfrage, die die Optionen des Servers vor dem eigentlichen Zugriff erfragen soll. Aus Performanzgründen soll die Abfrage jedoch cacheable sein. Wie könnten Sie dafür eine Lösung angehen?"
                                        }>
                                        Im Regelfall cached der Browser die Options-Anfrage für einen kurzen Zeitraum
                                        selbst. Hier lässt sich der Header Access-Control-Max-Age verwenden um die Zeit
                                        vorzugeben. Dennoch gibt es hier bei gewissen Anbietern Limitationen. So cached
                                        Google Chrome die Options Anfrage nur für maximal 10 Minuten - ganz egal welcher
                                        Wert dem Access-Control-Max-Age Header zugewiesen wurde.
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div className={"mt-5"}>
                            <ExerciseGroup task={"HTML-Literatur lesen und Fragen beantworten"} taskNumber={"1.3"}>
                                <div className={"mb-4"}>
                                    <Exercise task={"Was ist HTML?"}>
                                        Hypertext-Markup-Language. Es beschreibt den Aufbau einer Webseite - die reine
                                        Struktur. Gleichzeitig ist es möglich durch das style Attribut sofort CSS in
                                        HTML einzubetten, davon wird aber in der Regel abgeraten.
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Wie kann man eine geschachtelte geordnete Liste der Schachtelungstiefe 3 erzeugen?"
                                        }>
                                        Lösung:
                                        <Code language={"html"}>
                                            {`<ol>
    <li>
        <ol>
            <li>
                <ol>
                    <li>Schachtelungstiefe 3</li>
                </ol>
            </li>
        </ol>
    </li>
</ol>`}
                                        </Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise task={"Wie ist eine HTML-Tabelle aufgebaut?"}>
                                        Lösung:
                                        <Code language={"html"}>
                                            {`
<table>
    <thead>
        <tr>
            <th>Der Kopf der Tabelle (eine Spalte)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Der Body der Tabelle (eine Spalte)</td>
        </tr>
    </tbody>
</table>
`}
                                        </Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={"Welche Konventionen sollte man bei Pfaden und Dateinamen beachten?"}>
                                        Es sollten sich keine Sonderzeichen in Dateinamen oder Pfaden enthalten sein.
                                        Nur der Bindestrich ist erlaubt.
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise task={"Wie baut man in HTML ein Menü?"}>
                                        Lösung:
                                        <Code language={"html"}>
                                            {`<nav>
  <a href="...">...</a>...
</nav>`}
                                        </Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise task={"Welche Attribute sollte man bei Bildern wie verwenden?"}>
                                        Das alt Attribute sollte verwendet werden um Screenreader zu unterstützen, oder
                                        wenigstens einen Text anzuzeigen, sollte es zu Fehlern beim Laden des Bildes
                                        kommen. Auch ist es von Vorteil das &quot;width&quot; und &quot;height&quot;
                                        Attribut anzugeben, um unvorhergesehenen Skalierungen vorzubeugen.
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div className={"mt-5"}>
                            <ExerciseGroup task={"HTML Wireframe"} taskNumber={"1.4"}>
                                <div className={"mb-4"}>
                                    <Exercise task={"Wireframe nachbilden:"}>
                                        Lösung:
                                        <Code language={"html"}>
                                            {`<!DOCTYPE html>
<html lang="de">
    <head>
        <title>Ü1.2</title>
    </head>

    <body>
        <h1>Übung 1.2: Inventors of the Web</h1>
        <ul>
            <li>
                <b>
                    <mark>
                        <a href="https://kaul.inf.h-brs.de/we/assets/img/tbl.jpg">
                            Tim Berners-Lee:
                        </a>
                    </mark>
                </b>{" "}
                WWW, HTTP, HTML, URI
            </li>
            <li>
                <b>Hakom Lie and Bert Bos:</b> CSS
            </li>
            <li>
                <b>Brendan Eich:</b> JavaScript
            </li>
        </ul>
        <hr />
        <h2>Inventors of the WWW</h2>
        <table>
            <thead>
                <tr>
                    <th colSpan="4">Inventors of the WWW</th>
                </tr>
                <tr>
                    <th>WWW</th>
                    <th>HTML</th>
                    <th>CSS</th>
                    <th>JavaScript</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <mark>Tim Berners-Lee</mark>
                    </td>
                    <td>
                        <mark>Tim Berners-Lee</mark>
                    </td>
                    <td>Hakom Lie and Bert Bos</td>
                    <td>Brendan Eich</td>
                </tr>
            </tbody>
        </table>
        <hr />
        <table>
            <thead>
                <tr>
                    <th colSpan="2">
                        Inventors of the WWW
                        <hr />
                    </th>
                </tr>
                <tr>
                    <th>HTML</th>
                    <th>JavaScript</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <img
                            alt="Tim Berners-Lee"
                            src="https://kaul.inf.h-brs.de/we/assets/img/tbl.jpg"
                        />
                    </td>
                    <td>
                        <img
                            alt="Brendan Eich"
                            src="https://kaul.inf.h-brs.de/we/assets/img/eich.jpg"
                        />
                    </td>
                </tr>
                <tr>
                    <td>
                        <mark>Tim Berners-Lee</mark>
                    </td>
                    <td>Brendan Eich</td>
                </tr>
            </tbody>
        </table>
        <hr />
    </body>
</html>`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                    </div>
                </div>
            </div>
        </Page>
    );
};

export default U1;
