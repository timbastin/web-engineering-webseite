import React from "react";
import Page from "../components/Page";
import Exercise from "../components/Exercise";
import ExerciseGroup from "../components/ExerciseGroup";
import Sidebar from "../components/Sidebar";
import Code from "../components/Code";

const links = [
    {
        anchor: "6.1",
        title: "Funktionen in JavaScript",
    },
    {
        anchor: "6.2",
        title: "Textanalyse mit filter-map-reduce",
    },
];
const U6 = () => {
    return (
        <Page title={"Übung 6 - Funktional"}>
            <div className={"container"}>
                <div className={"row"}>
                    <div className={"col-md-3"}>
                        <Sidebar links={links} />
                    </div>
                    <div className={"col-md-9"}>
                        <div>
                            <ExerciseGroup task={"Funktionen in JavaScript "} taskNumber={"6.1"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie eine Funktion identity_function(), die ein Argument als Parameter entgegen nimmt und eine Funktion zurück gibt, die dieses Argument zurück gibt."
                                        }>
                                        <Code
                                            noIframe={true}
                                            language={"javascript"}>{`const identity_function = (x) => x;`}</Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie eine Addier-Funktion addf(), so dass addf(x)(y) genau x + y zurück gibt. (Es haben also zwei Funktionsaufrufe zu erfolgen. addf(x) liefert eine Funktion, die auf y angewandt wird.)"
                                        }>
                                        <Code
                                            noIframe={true}
                                            language={"javascript"}>{`const addf = (x) => (y) => x + y;`}</Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie eine Funktion applyf(), die aus einer binären Funktion wie add(x,y) eine Funktion addfberechnet, die mit zwei Aufrufen das gleiche Ergebnis liefert, z.B. addf = applyf(add); addf(x)(y) soll add(x,y) liefern. Entsprechend applyf(mul)(5)(6) soll 30 liefern, wenn mul die binäre Multiplikation ist."
                                        }>
                                        <Code
                                            noIframe={true}
                                            language={
                                                "javascript"
                                            }>{`const applyf = (binaryFn) => (x) => (y) => binaryFn(x,y);`}</Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie eine Funktion curry() (von Currying), die eine binäre Funktion und ein Argument nimmt, um daraus eine Funktion zu erzeugen, die ein zweites Argument entgegen nimmt, z.B. add3 = curry(add, 3);add3(4) ergibt 7. curry(mul, 5)(6) ergibt 30."
                                        }>
                                        <Code
                                            noIframe={true}
                                            language={
                                                "javascript"
                                            }>{`const curry = (binaryFn, x) => (y) => binaryFn(x,y);`}</Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Erzeugen Sie die inc-Funktion mit Hilfe einer der Funktionen addf, applyf und curry aus den letzten Aufgaben, ohne die Funktion inc() selbst zu implementieren. (inc(x) soll immer x + 1 ergeben und lässt sich natürlich auch direkt implementieren. Das ist aber hier nicht die Aufgabe.) Vielleicht schaffen Sie es auch, drei Varianten der inc()-Implementierung zu schreiben?"
                                        }>
                                        <Code noIframe={true} language={"javascript"}>{`inc = addf(1);`}</Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie eine Funktion methodize(), die eine binäre Funktion (z.B. add, mul) in eine unäre Methode verwandelt. Nach Number.prototype.add = methodize(add); soll (3).add(4) genau 7 ergeben."
                                        }>
                                        <Code
                                            noIframe={true}
                                            language={"javascript"}>{`const methodize = function (fn) {
  return function (x) {
    fn(this, x);
  }
}`}</Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie eine Funktion demethodize(), die eine unäre Methode (z.B. add, mul) in eine binäre Funktion umwandelt. demethodize(Number.prototype.add)(5, 6) soll 11 ergeben."
                                        }>
                                        <Code
                                            noIframe={true}
                                            language={"javascript"}>{`const demethodize = function (fn) {
  return function (x,y) {
    const bound = fn.bind(x)
    return bound(y);
  }
}`}</Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie eine Funktion twice(), die eine binäre Funktion in eine unäre Funktion umwandelt, die den einen Parameter zweimal weiter reicht. Z.B. var double = twice(add); double(11) soll 22 ergeben; var square = twice(mul); square(11) soll mul(11,11) === 121 ergeben."
                                        }>
                                        <Code
                                            noIframe={true}
                                            language={"javascript"}>{`const twice = (fn) => (x) => fn(x,x);`}</Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie eine Funktion composeu(), die zwei unäre Funktionen in eine einzelne unäre Funktion transformiert, die beide nacheinander aufruft, z.B. soll composeu(double, square)(3) genau 36 ergeben."
                                        }>
                                        <Code
                                            noIframe={true}
                                            language={
                                                "javascript"
                                            }>{`const composeu = (fn1, fn2) => (x) => fn2(fn1(x));`}</Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie eine Funktion composeb(), die zwei binäre Funktionen in eine einzelne Funktion transformiert, die beide nacheinander aufruft, z.B. composeb(add, mul)(2, 3, 5) soll 25 ergeben."
                                        }>
                                        <Code
                                            noIframe={true}
                                            language={
                                                "javascript"
                                            }>{`const composeb = (fn1, fn2) => (x,y,z) => fn2(fn1(x, y), z);`}</Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie eine Funktion once(), die einer anderen Funktion nur einmal erlaubt, aufgerufen zu werden, z.B. add_once = once(add); add_once(3, 4) soll beim ersten Mal 7 ergeben, beim zweiten Mal soll jedoch add_once(3, 4) einen Fehlerabbruch bewirken."
                                        }>
                                        <Code noIframe={true} language={"javascript"}>{`const once = (fn) => {
  fn.times = 0
  return (x,y) => {
    if (fn.times > 0) {
      throw new Error();
    }
    fn.times ++;
    return fn(x,y)
  }
}`}</Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie eine Fabrik-Funktion counterf(), die zwei Funktionen inc() und dec() berechnet, die einen Zähler hoch- und herunterzählen. Z.B. counter = counterf(10); Dann soll counter.inc() 11 und counter.dec() wieder 10 ergeben.\n"
                                        }>
                                        <Code noIframe={true} language={"javascript"}>{`function counterf(c) {
  let counter = c;
  return {
    inc() {
      counter ++;
      return counter;
    },
    dec() {
      counter --;
      return counter;
    }
  }
}`}</Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie eine rücknehmbare Funktion revocable(), die als Parameter eine Funktion nimmt und diese bei Aufruf ausführt. Sobald die Funktion aber mit revoke() zurück genommen wurde, führt ein erneuter Aufruf zu einem Fehler."
                                        }>
                                        <Code noIframe={true} language={"javascript"}>{`function revocable(fn) {
  let allow = true
  return {
    invoke(x) {
      if (allow) {
        return fn(x);
      }
      throw new Error();
    },
    revoke() {
      allow = false;
    }
  }
}`}</Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            'Implementieren Sie ein "Array Wrapper"-Objekt mit den Methoden get, store und append, so dass ein Angreifer keinen Zugriff auf das innere, private Array hat.\n'
                                        }>
                                        <Code noIframe={true} language={"javascript"}>{`function vector() {
  const arr = [];
  return {
    append(x) {
      arr.push(x)
    },
    store(index, x) {
      arr[index] = x;
    },
    get(index) {
      return arr[index]
    }
  }
}`}</Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div className={"mt-5"}>
                            <ExerciseGroup task={"Textanalyse mit filter-map-reduce"} taskNumber={"6.2"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie in JavaScript eine Textanalyse. Ermitteln Sie die häufigsten Begriffe im Text Plagiatsresolution. Filtern Sie dabei alle Stoppworte und HTML-Tags. Reduzieren Sie das Ergebnis auf die 3 häufigsten Begriffe."
                                        }>
                                        <Code noIframe={true} language={"javascript"}>
                                            {`const stoppworte = \`ab
aber
alle
allein
allem
allen
aller
allerdings
allerlei
alles
allmählich
allzu
als
alsbald
also
am
an
and
ander
andere
anderem
anderen
anderer
andererseits
anderes
anderm
andern
andernfalls
anders
anstatt
auch
auf
aus
ausgenommen
ausser
ausserdem
außer
außerdem
außerhalb
bald
bei
beide
beiden
beiderlei
beides
beim
beinahe
bereits
besonders
besser
beträchtlich
bevor
bezüglich
bin
bis
bisher
bislang
bist
bloß
bsp.
bzw
ca
ca.
content
da
dabei
dadurch
dafür
dagegen
daher
dahin
damals
damit
danach
daneben
dann
daran
darauf
daraus
darin
darum
darunter
darüber
darüberhinaus
das
dass
dasselbe
davon
davor
dazu
daß
dein
deine
deinem
deinen
deiner
deines
dem
demnach
demselben
den
denen
denn
dennoch
denselben
der
derart
derartig
derem
deren
derer
derjenige
derjenigen
derselbe
derselben
derzeit
des
deshalb
desselben
dessen
desto
deswegen
dich
die
diejenige
dies
diese
dieselbe
dieselben
diesem
diesen
dieser
dieses
diesseits
dir
direkt
direkte
direkten
direkter
doch
dort
dorther
dorthin
drauf
drin
drunter
drüber
du
dunklen
durch
durchaus
eben
ebenfalls
ebenso
eher
eigenen
eigenes
eigentlich
ein
eine
einem
einen
einer
einerseits
eines
einfach
einführen
einführte
einführten
eingesetzt
einig
einige
einigem
einigen
einiger
einigermaßen
einiges
einmal
eins
einseitig
einseitige
einseitigen
einseitiger
einst
einstmals
einzig
entsprechend
entweder
er
erst
es
etc
etliche
etwa
etwas
euch
euer
eure
eurem
euren
eurer
eures
falls
fast
ferner
folgende
folgenden
folgender
folgendes
folglich
fuer
für
gab
ganze
ganzem
ganzen
ganzer
ganzes
gar
gegen
gemäss
ggf
gleich
gleichwohl
gleichzeitig
glücklicherweise
gänzlich
hab
habe
haben
haette
hast
hat
hatte
hatten
hattest
hattet
heraus
herein
hier
hier
hinter
hiermit
hiesige
hin
hinein
hinten
hinter
hinterher
http
hätt
hätte
hätten
höchstens
ich
igitt
ihm
ihn
ihnen
ihr
ihre
ihrem
ihren
ihrer
ihres
im
immer
immerhin
in
indem
indessen
infolge
innen
innerhalb
ins
insofern
inzwischen
irgend
irgendeine
irgendwas
irgendwen
irgendwer
irgendwie
irgendwo
ist
ja
je
jed
jede
jedem
jeden
jedenfalls
jeder
jederlei
jedes
jedoch
jemand
jene
jenem
jenen
jener
jenes
jenseits
jetzt
jährig
jährige
jährigen
jähriges
kam
kann
kannst
kaum
kein
keine
keinem
keinen
keiner
keinerlei
keines
keineswegs
klar
klare
klaren
klares
klein
kleinen
kleiner
kleines
koennen
koennt
koennte
koennten
komme
kommen
kommt
konkret
konkrete
konkreten
konkreter
konkretes
können
könnt
künftig
leider
machen
man
manche
manchem
manchen
mancher
mancherorts
manches
manchmal
mehr
mehrere
mein
meine
meinem
meinen
meiner
meines
mich
mir
mit
mithin
muessen
muesst
muesste
muss
musst
musste
mussten
muß
mußt
müssen
müsste
müssten
müßt
müßte
nach
nachdem
nachher
nachhinein
nahm
natürlich
neben
nebenan
nehmen
nein
nicht
nichts
nie
niemals
niemand
nirgends
nirgendwo
noch
nun
nur
nächste
nämlich
nötigenfalls
ob
oben
oberhalb
obgleich
obschon
obwohl
oder
oft
per
plötzlich
schließlich
schon
sehr
sehrwohl
seid
sein
seine
seinem
seinen
seiner
seines
seit
seitdem
seither
selber
selbst
sich
sicher
sicherlich
sie
sind
so
sobald
sodass
sodaß
soeben
sofern
sofort
sogar
solange
solch
solche
solchem
solchen
solcher
solches
soll
sollen
sollst
sollt
sollte
sollten
solltest
somit
sondern
sonst
sonstwo
sooft
soviel
soweit
sowie
sowohl
tatsächlich
tatsächlichen
tatsächlicher
tatsächliches
trotzdem
ueber
um
umso
unbedingt
und
unmöglich
unmögliche
unmöglichen
unmöglicher
uns
unser
unser
unsere
unsere
unserem
unseren
unserer
unseres
unter
usw
viel
viele
vielen
vieler
vieles
vielleicht
vielmals
vom
von
vor
voran
vorher
vorüber
völlig
wann
war
waren
warst
warum
was
weder
weil
weiter
weitere
weiterem
weiteren
weiterer
weiteres
weiterhin
weiß
welche
welchem
welchen
welcher
welches
wem
wen
wenig
wenige
weniger
wenigstens
wenn
wenngleich
wer
werde
werden
werdet
weshalb
wessen
wichtig
wie
wieder
wieso
wieviel
wiewohl
will
willst
wir
wird
wirklich
wirst
wo
wodurch
wogegen
woher
wohin
wohingegen
wohl
wohlweislich
womit
woraufhin
woraus
worin
wurde
wurden
während
währenddessen
wär
wäre
wären
würde
würden
z.B.
zB
zahlreich
zeitweise
zu
zudem
zuerst
zufolge
zugleich
zuletzt
zum
zumal
zur
zurück
zusammen
zuviel
zwar
zwischen
ähnlich
übel
über
überall
überallhin
überdies
übermorgen
übrig
übrigens\`.split("\\n")

const str = \`Plagiatsresolution und -maßnahmen
Resolution zum akademischen Ethos und zu den akademischen Standards

In guter Tradition und anlässlich der öffentlichen Diskussion zum Plagiatsthema sieht sich die Hochschule Bonn-Rhein-Sieg in der Pflicht, ihre Position klar und eindeutig zu bekunden und hochschulweit Maßnahmen einzuleiten.

1. Die Hochschule Bonn-Rhein-Sieg bekennt sich mit dieser Resolution öffentlich zum akademischen Ethos und den akademischen Standards.

2. Die Hochschule Bonn-Rhein-Sieg sieht sich verpflichtet, alle Studierende frühzeitig im Studium sowohl über den wissenschaftlichen Auftrag und den akademischen Ethos als auch über die Konsequenzen seiner Missachtung aufzuklären. In allen Studiengängen wird intensiv in die wissenschaftliche Arbeits- und Denkweise eingeführt und über den akademischen Ethos und die akademischen Standards klar und eindeutig aufgeklärt.

3. In einer Selbstverpflichtungserklärung zum akademischen Ethos geben alle Studierende der Hochschule Bonn-Rhein-Sieg spätestens gegen Ende des ersten Studienjahres zum Ausdruck, dass sie sich von den Dozentinnen und Dozenten der Hochschule Bonn-Rhein-Sieg hinreichend über den akademischen Ethos und die akademischen Standards aufgeklärt sind und diese beachten werden.

Der Senat befürwortete in seiner Sitzung am 03.05.2012 die Resolution in der o.g. Fassung.

Eckpunkte zur Plagiatsprüfung

Der Senat empfiehlt:

1. Die Aufklärung und das Bekenntnis zum akademischen Ethos und den akademischen Standards muss fester Bestandteil aller Curricula aller Studiengänge im ersten Studienjahr sein. Alle Curricula aller Studiengänge werden darauf hin geprüft und ggfs. ergänzt.

2. Alle Abschlussarbeiten werden auf Plagiate geprüft.

3. Alle Abschlussarbeiten mit Plagiaten werden grundsätzlich als Fehlversuch gewertet.

4. Die Entscheidung, ob die Arbeit Plagiate enthält, liegt zuerst bei den Gutachterinnen und Gutachtern. Der Nachweis in einem Gutachten reicht.

5. Alle Abschlussarbeiten werden grundsätzlich auch in elektronischer Form (PDF-Format und Originalformat wie Word, OpenOffice, ...) eingereicht.

6. Alle Abschlussarbeiten ohne Sperrvermerk werden einem vom Fachbereich definierten Kreis zur Einsicht zur Verfügung gestellt. Alle Abschlussarbeiten sollten nach Möglichkeit grundsätzlich zur Veröffentlichung freigegeben werden. Wissenschaft zielt auf Veröffentlichung ab. Nichtveröffentlichung sollte nur in begründeten und durch den Prüfungsausschuss genehmigten Ausnahmefällen geschehen.

7. Im Bereich von Seminar-, Hausarbeiten und Praktikumsberichten behält sich die Hochschule stichprobenartige Plagiatsprüfungen vor.

Selbstverpflichtungserklärung der Studierenden:

Eine akademische Arbeit stellt eine individuelle Leistung dar, die eigenständig und allein auf Basis der im Literaturverzeichnis angegebenen Quellen erstellt wurde und in der alle Zitate als solche gekennzeichnet sind.

"Ich erkläre hiermit, dass ich den akademischen Ehrencodex kenne und über die Folgen einer Missachtung oder Verletzung aufgeklärt worden bin."\`

const counts = (str.split(" ").filter(word => !stoppworte.includes(word)).reduce(function (acc, curr) {
  if (typeof acc[curr] == 'undefined') {
    acc[curr] = 1;
  } else {
    acc[curr] += 1;
  }

  return acc;
}, {}));

const objectKeys = Object.keys(counts);
let highestCounts = [counts[objectKeys[0]],counts[objectKeys[1]],counts[objectKeys[2]]];
let highestKeys = [objectKeys[0], objectKeys[1], objectKeys[2]];
const getSmallest = (arr) => {
  let smallest = arr[0];
  let smallestIndex = 0;
  arr.forEach((el, i) => {
    if (el < smallest) {
      smallest = el;
      smallestIndex = i;
    }
  });
  return [smallest, smallestIndex];
}
Object.keys(counts).forEach(key => {
  const [smallest, smallestIndex] = getSmallest(highestCounts);
  if (counts[key] > smallest && !highestKeys.includes(key)) {
    highestCounts[smallestIndex] = counts[key];
    highestKeys[smallestIndex] = key;
  }
});
console.log(highestKeys, highestCounts)`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                    </div>
                </div>
            </div>
        </Page>
    );
};

export default U6;
