import React from "react";
import Page from "../components/Page";
import Exercise from "../components/Exercise";
import ExerciseGroup from "../components/ExerciseGroup";
import Sidebar from "../components/Sidebar";
import Code from "../components/Code";

const links = [
    {
        anchor: "5.1",
        title: "Klasse für Vorrangrelationen",
    },
    {
        anchor: "5.2",
        title: "Topologische Iterierbarkeit",
    },
    {
        anchor: "5.3",
        title: "Topologischer Generator",
    },
    {
        anchor: "5.4",
        title: "Proxy",
    },
    {
        anchor: "5.5",
        title: "DeepCopy",
    },
];
const U5 = () => {
    return (
        <Page title={"Übung 5 - ECMA Script"}>
            <div className={"container"}>
                <div className={"row"}>
                    <div className={"col-md-3"}>
                        <Sidebar links={links} />
                    </div>
                    <div className={"col-md-9"}>
                        <div>
                            <ExerciseGroup task={"Klasse für Vorrangrelationen"} taskNumber={"5.1"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            'Schreiben Sie eine ES6-Klasse Vorrang für Vorrangrelationen, z.B. new Vorrang([ ["schlafen", "studieren"], ["essen", "studieren"], ["studieren", "prüfen"] ]). Wählen Sie eine Implementierung, die universell gültig, also nicht nur für dieses Beispiel gilt. (Überlegen Sie sich, über welche Properties und Methoden eine solche Klasse verfügen sollte und wie TopSort hier hinein spielt. Topologische Iterierbarkeit und topologischer Generator sind jedoch Gegenstand der nächsten Übungen weiter unten auf diesem Übungsblatt und sollten für diese Aufgaben aufgespart werden.)\n' +
                                            "\n" +
                                            "Verwenden Sie die neuen Sprach-Konzepte aus der Vorlesung so weit wie möglich."
                                        }>
                                        <Code noIframe={true} language={"javascript"}>
                                            {`class Vorrang {
    arr;
    solution;
    constructor(arr) {
        this.arr = arr;
        this.calculate();
    }
    private calculate() {
        this.solution = this.topsort(this.arr);
    }
    private hasNoDependency(el, arr) {
        return !arr.some(deps => deps.length > 1 && deps[1] === el);
    }
    private topsort(arr) {
        const sort = [];
        while (arr.length > 0) {
            arr.forEach(deps => {
                if (this.hasNoDependency(deps[0], arr)) {
                    sort.push(deps[0]);
                    // remove it from arr.
                    arr = arr.map(d => d.filter(task => task !== deps[0]));
                    arr = arr.filter(d => d.length > 0);
                }
            });
        }
        return sort;
    }
}
const vorrang = new Vorrang([
    ["schlafen", "studieren"],
    ["essen", "studieren"],
    ["studieren", "prüfen"],
]);
console.assert(vorrang.solution.indexOf("schlafen") < vorrang.solution.indexOf("essen"));
console.assert(vorrang.solution.indexOf("essen") < vorrang.solution.indexOf("studieren"));
console.assert(vorrang.solution.indexOf("studieren") < vorrang.solution.indexOf("prüfen"));`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div className={"mt-5"}>
                            <ExerciseGroup task={"Topologische Iterierbarkeit"} taskNumber={"5.2"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Stellen Sie bei Ihrer Klasse aus der letzten Aufgabe die topologische Iterierbarkeit her (zunächst über das Iterationsprotokoll, ohne Generator, ohne yield).\n" +
                                            "\n" +
                                            "Zum Beispiel soll dadurch folgende for ... of loop möglich werden, mit der die Elemente in topologischer Sortierung durchlaufen werden."
                                        }>
                                        <Code noIframe={true} language={"javascript"}>
                                            {`class Vorrang {
  arr;
  solution;
  constructor(arr) {
      this.arr = arr;
  }
  
  next() {
      for (const deps of this.arr) {
          if (this.hasNoDependency(deps[0], this.arr)) {
              // remove it from arr.
              const value = deps[0];
              this.arr = this.arr.map(d => d.filter(task => task !== value));
              this.arr = this.arr.filter(d => d.length > 0);
              return { value, done: false };
          }
      }
      //
      return { value: undefined, done: true };
  }
  
  private hasNoDependency(el, arr) {
      return !arr.some(deps => deps.length > 1 && deps[1] === el);
  }
  
  [Symbol.iterator]() {
      return this;
  }
}

const vorrang = new Vorrang([
  ["schlafen", "studieren"],
  ["essen", "studieren"],
  ["studieren", "prüfen"],
]);

const solution = ["schlafen", "essen", "studieren", "prüfen"];
let index = 0;
for (const el of vorrang) {
console.assert(solution[index] === el);
index++;
`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div>
                            <ExerciseGroup task={"Topologischer Generator"} taskNumber={"5.3"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Stellen Sie bei Ihrer Klasse aus der vorletzten Aufgabe die topologische Iterierbarkeit mittels Generator her.\n" +
                                            "\n" +
                                            "Wählen Sie eine Implementierung, die universell gültig, also nicht nur für das Beispiel gilt.\n" +
                                            "\n"
                                        }>
                                        <Code noIframe={true} language={"javascript"}>
                                            {`class Vorrang {
    arr;
    solution;
    cursor;
    constructor(arr) {
        this.arr = arr;
        this.cursor = 0;
        this.calculate();
    }
    private calculate() {
        this.solution = this.topsort(this.arr);
    }

    public *sol() {
        yield this.solution[this.cursor];
        this.cursor++;
    }
    private hasNoDependency(el, arr) {
        return !arr.some(deps => deps.length > 1 && deps[1] === el);
    }
    private topsort(arr) {
        const sort = [];
        while (arr.length > 0) {
            arr.forEach(deps => {
                if (this.hasNoDependency(deps[0], arr)) {
                    sort.push(deps[0]);
                    // remove it from arr.
                    arr = arr.map(d => d.filter(task => task !== deps[0]));
                    arr = arr.filter(d => d.length > 0);
                }
            });
        }
        return sort;
    }
}
const vorrang = new Vorrang([
    ["schlafen", "studieren"],
    ["essen", "studieren"],
    ["studieren", "prüfen"],
]);

const solution = ["schlafen", "essen", "studieren", "prüfen"];
let index = 0;
for (const el of vorrang.sol()) {
    console.assert(solution[index] === el);
    index++;
}`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div>
                            <ExerciseGroup task={"Proxy"} taskNumber={"5.4"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Erweitern Sie Ihre Vorrang-Klasse um Logging, indem Sie ein Proxy einfügen. Lassen Sie sich vom Logger bei jedem Schritt ausgeben, wie viele der Vorrangrelationen noch übrig bleiben. Verwenden Sie so weit wie möglich High-Level-Methoden wie Object.keys und High-Level-Datenstrukturen wie Map und Set und deren Methoden, anstatt mühsam von Hand zu iterieren und zu zählen."
                                        }>
                                        <Code noIframe={true} language={"javascript"}>
                                            {`class Vorrang {
  arr;
  solution;
  cursor;
  constructor(arr) {
      this.arr = arr;
      this.cursor = 0;
      this.calculate();
  }

  private calculate() {
      this.solution = this.topsort(this.arr);
  }

  public nextElement() {
      const el = this.solution[this.cursor];
      this.cursor++;
      return el;
  }

  private hasNoDependency(el, arr) {
      return !arr.some(deps => deps.length > 1 && deps[1] === el);
  }
  private topsort(arr) {
      const sort = [];
      while (arr.length > 0) {
          arr.forEach(deps => {
              if (this.hasNoDependency(deps[0], arr)) {
                  sort.push(deps[0]);
                  // remove it from arr.
                  arr = arr.map(d => d.filter(task => task !== deps[0]));
                  arr = arr.filter(d => d.length > 0);
              }
          });
      }
      return sort;
  }
}
const vorrang = new Vorrang([
  ["schlafen", "studieren"],
  ["essen", "studieren"],
  ["studieren", "prüfen"],
]);
const proxy = new Proxy(vorrang, {
  get(target, p): any {
      if (p === "nextElement") {
          console.log("Elements left: ", target.solution.length - (target.cursor + 1));
      }
      return target[p];
  },
});
console.log(proxy.nextElement());`}
                                        </Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Testen Sie Ihren JavaScript-Code. Verwenden Sie für Ihre Tests die Funktion console.assert. Geben Sie hier Ihre Tests ein:"
                                        }>
                                        <Code noIframe={true} language={"html"}>{`<!doctype html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <title>Topologische Sortierung</title>
  </head>
  
  <body>
    <h1>Topologische Sortierung</h1>
    <script>
      function topsort(arr) {
        function hasNoDependency(el, arr) {
          return !arr.some(deps => deps.length > 1 && deps[1] === el);
        }
      
        var sort = []
        while(arr.length > 0) {
          arr.forEach((deps) => {
            if (hasNoDependency(deps[0], arr)) {
              sort.push(deps[0])
              // remove it from arr.
              arr = arr.map(d => d.filter(task => task !== deps[0]));
              arr = arr.filter(d => d.length > 0);
            }
          })
        }
        return sort;
      }
      console.log(topsort([ ["essen", "studieren"], ["schlafen", "studieren"], ["studieren", "prüfen"] ]));
      
      var result = topsort([ ["essen", "studieren"], ["schlafen", "studieren"], ["studieren", "prüfen"] ])
      console.assert(result.indexOf("essen") < result.indexOf("studieren"))
      console.assert(result.indexOf("essen") < result.indexOf("prüfen"))
      console.assert(result.indexOf("studieren") < result.indexOf("prüfen"))
    </script>
  </body>
</html>`}</Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>

                        <div>
                            <ExerciseGroup task={"DeepCopy"} taskNumber={"5.5"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie eine rekursive Funktion deepCopy( struct ) als ES6-Ausdruck, so dass beliebig geschachtelte Arrays und Objekte struct tiefenkopiert werden können. Verwenden Sie zu diesem Zweck den konditionalen ternären Operator, Array.map(), Object.fromEntries() und Object.entries(). Verwenden Sie dabei nur Arrow Functions und Ausdrücke, keine Anweisungen, keine Blöcke. Verwenden Sie nicht die JSON-Methoden."
                                        }>
                                        <Code noIframe={true} language={"javascript"}>
                                            {`const deepCopy = struct => {
  return struct instanceof Array
      ? struct.map(deepCopy)
      : typeof struct == "object"
      ? Object.fromEntries(struct.entries(deepCopy))
      : struct;
};

const arr = [[1, 2, 3]];

const other = deepCopy(arr);
other[0][0] = 2;

console.assert(other[0][0] === 2);
console.assert(arr[0][0] === 1);`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                    </div>
                </div>
            </div>
        </Page>
    );
};

export default U5;
