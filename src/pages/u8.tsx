import React from "react";
import Page from "../components/Page";
import Exercise from "../components/Exercise";
import ExerciseGroup from "../components/ExerciseGroup";
import Sidebar from "../components/Sidebar";
import Code from "../components/Code";

const links = [
    {
        anchor: "8.1",
        title: "Promises",
    },
    {
        anchor: "8.2",
        title: "async / await",
    },
    {
        anchor: "8.3",
        title: "WWW-Navigator",
    },
];
const U8 = () => {
    return (
        <Page title={"Übung 8 - Async"}>
            <div className={"container"}>
                <div className={"row"}>
                    <div className={"col-md-3"}>
                        <Sidebar links={links} />
                    </div>
                    <div className={"col-md-9"}>
                        <div>
                            <ExerciseGroup task={"Promises"} taskNumber={"8.1"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Erstellen Sie auf Ihrem Server www2.inf.h-brs.de (oder localhost) zwei Text-Dateien A.txt und B.txt mit ungefähr gleich vielen Zeilen. Laden Sie mit der fetch-API parallel beide Text-Dateien vom Server. Geben Sie auf einer Webseite den Inhalt beider Dateien zeilenweise aus, wobei der Anfang der Zeile aus A.txt und das Ende aus B.txt stammen soll. Die beiden Dateien sollen also zeilenweise konkateniert werden. Erzielen Sie max. Geschwindigkeit durch maximale Parallelität. Achten Sie gleichzeitig auf Korrektheit. Verwenden Sie dabei ausschließlich die Promise API ohne async / await."
                                        }>
                                        <Code language={"html"}>{`<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>8.1 Promises</title>
</head>
<body id="body">
<div id="content" />
<script>
  Promise.all([
    fetch("http://www2.inf.h-brs.de/~tbasti2s/A.txt"),
    fetch("http://www2.inf.h-brs.de/~tbasti2s/B.txt")
  ]).then(([a, b]) => {
    return Promise.all([a.text(), b.text()])
  }).then(([a,b]) => {
    const aArray = a.split("\\n");
    const bArray = b.split("\\n")
    let str = "";
    for (let i = 0; i < aArray.length; i ++) {
      if (!bArray.length > i) {
        break;
      }
      str += aArray[i] + bArray[i] + "\\n";
    }
    document.getElementById("content").innerText = str;
  })
</script>
</body>
</html>
`}</Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div className={"mt-5"}>
                            <ExerciseGroup task={"async / await"} taskNumber={"8.2"}>
                                <div className={"mb-4"}>
                                    <Exercise task={"Lösen Sie die letzte Aufgabe mit async / await statt Promise."}>
                                        <Code language={"html"}>
                                            {`<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>8.1 Promises</title>
</head>
<body id="body">
<div id="content" />
<script>
  async function foo() {
    const [a, b] = await Promise.all([
    fetch("http://www2.inf.h-brs.de/~tbasti2s/A.txt"),
    fetch("http://www2.inf.h-brs.de/~tbasti2s/B.txt")
  ]);
  const [aText, bText] = await Promise.all([a.text(), b.text()])
  const aArray = aText.split("\\n");
  const bArray = bText.split("\\n")
  let str = "";
  for (let i = 0; i < aArray.length; i ++) {
    if (!bArray.length > i) {
      break;
    }
    str += aArray[i] + bArray[i] + "\\n";
  }
  document.getElementById("content").innerText = str;
  }
  foo();
</script>
</body>
</html>

`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div className={"mt-5"}>
                            <ExerciseGroup task={"WWW-Navigator"} taskNumber={"8.3"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Geben Sie die komplette HTML-Seite für den WWW-Navigator exkl. JavaScript-Quelltext an:"
                                        }>
                                        <Code language={"html"}>
                                            {`<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>8.3 WWW-Navigator</title>
  <style>
    a {
        cursor: pointer;
        word-break: break-all;
    }
    header {
        background-color: white;
    }
    html, body {
        background-color: #eeee;
        margin: 0;
        font-family: sans-serif;
    }
    h1 {
        margin-top: 0;
    }
    header {
        border-bottom: 1px solid rgba(0,0,0,0.3);
        padding: 10px 20px 0 200px;
    }
    header a {
        padding: 5px 10px;
        width: 200px;
        display: block;
        text-align: center;
     
    }
    nav {
        display: flex;
    }
    .active-sub {
        background-color: rgba(159, 198, 255, 0.5);
    }
    .active {
        border-bottom: 2px solid rgba(159, 198, 255, 1);
    }
    #wrapper {
        display: flex;
    }
    #references {
        min-width: 200px;
        max-width: 200px;
        padding: 10px 20px;
        border-left: 1px solid rgba(0,0,0,0.3);
    }
    #content {
        width: calc(100% - 400px);
        padding: 10px 20px;
    }
    #sidenav a {
        display: block;
        border-bottom: 1px solid rgba(0,0,0,0.3);
        padding: 10px 20px;
    }
    #sidenav {
        border-right: 1px solid rgba(0,0,0,0.3);
        min-width: 200px;
        background-color: white;
        height: calc(100vh - 97px)
    }
  </style>
</head>
<body id="body">
<header>
  <h1>WWW-Navigator</h1>
  <nav>
    <a class="nav-button active" id="html">HTML</a>
    <a class="nav-button" id="css">CSS</a>
    <a class="nav-button" id="javascript">JavaScript</a>
  </nav>
</header>
<div id="wrapper">
  <div id="sidenav"></div>
  <div id="content"></div>
  <div id="references"></div>
</div>
<script>
  async function handleFile(fileName) {
    const response = await fetch("http://www2.inf.h-brs.de/~tbasti2s/" + fileName)
    const json = await response.json();
    document.getElementById("content").innerHTML = "";
    document.getElementById("sidenav").innerHTML = "";
    document.getElementById("references").innerHTML = "";
    document.getElementById("sidenav").append(...Object.keys(json).map(key => {
        const el = document.createElement("a");
        el.innerText = key;
        el.addEventListener("click", (event) => {
          const activeSubs = document.getElementsByClassName("active-sub");
          for (const activeSub of activeSubs) {
            activeSub.classList.remove("active-sub");
          }
          const h2 = document.createElement("h3")
          h2.innerText = "Erklärung"
          event.target.classList.add("active-sub");
          const content = document.getElementById("content");
          content.innerHTML = ""
          content.append(h2)
          content.append(json[key]["content"])
          document.getElementById("references").innerHTML = "";
          const h3 = document.createElement("h3")
          h3.innerText = "Quellen"
          document.getElementById("references").append(...[h3,...json[key]["references"].map(ref => {
            const a = document.createElement("a");
            a.text = ref
            a.href = ref;
            return a;
          })])
        })
        return el;
      }));
    }
    function hashChangeListener() {
      let hash = window.location.hash.substring(1);
      if (!["html", "javascript", "css"].includes(hash)) {
        hash = "html";
      }
      const active = document.getElementsByClassName("active");
      for (const a of active) {
        a.classList.remove("active");
      }
      const el = document.getElementById(hash);
      el.classList.add("active")
      handleFile(hash + ".json")
    }
    function clickHandler(event) {
        window.location.hash = event.target.id;
    }
    const elements = document.getElementsByClassName("nav-button");
    for (const el of elements) {
      el.addEventListener("click", clickHandler)
    }
    window.onhashchange = hashChangeListener;
    hashChangeListener()
</script>
</body>
</html>
`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                    </div>
                </div>
            </div>
        </Page>
    );
};

export default U8;
