import React from "react";
import Page from "../components/Page";
import Exercise from "../components/Exercise";
import ExerciseGroup from "../components/ExerciseGroup";
import Sidebar from "../components/Sidebar";
import Code from "../components/Code";

const links = [
    {
        anchor: "3.1",
        title: "Responsiv mit Flexbox Desktop-First",
    },
    {
        anchor: "3.2",
        title: "Responsiv mit Grid Mobile-First",
    },
    {
        anchor: "3.3",
        title: "Responsiv mit Grid",
    },
];
const U3 = () => {
    return (
        <Page title={"Übung 3 - CSS 2. Teil Responsive Web Design"}>
            <div className={"container"}>
                <div className={"row"}>
                    <div className={"col-md-3"}>
                        <Sidebar links={links} />
                    </div>
                    <div className={"col-md-9"}>
                        <div>
                            <ExerciseGroup task={"Responsiv mit Flexbox Desktop-First"} taskNumber={"3.1"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Spielen Sie zunächst das Flexbox Froggy-Spiel, um Flexbox zu lernen. Implementieren Sie dann ausschließlich mit HTML und CSS Flexbox folgendes responsive Webdesign nach der Desktop-First-Strategie!"
                                        }>
                                        <Code language={"html"}>
                                            {`<html>
<head>
  <style type="text/css">
    body, html {
    margin: 0;
  }
    div:not(.flex) {
    margin: 10px;
  }
    .red {
    background-color: red;
    min-height:200px;
  }
    .green {
    background-color: greenyellow;
    min-height:200px;
  }
    .green, .pink {
    flex: 1;
  }
    .blue {
    background-color: blue;
    min-height:1000px;
    flex: 3;
  }
    .pink {
    background-color: deeppink;
    min-height:200px;
  }
    .flex {
    display: flex;
  }

    @media (max-width: 768px) {
    .pink {
    flex: none;
    width: calc(100% - 20px);
  }
    .flex {
    flex-wrap: wrap;
  }
    .blue {
    flex: 3
  }
    .green {
    flex: 1
  }
  }
    @media (max-width: 576px) {
    .green {
    flex: none;
    width: calc(100% - 20px);
  }
  }
  </style>
</head>
<body>
<div class="red"></div>
<div class="flex">
  <div class="green"></div>
  <div class="blue"></div>
  <div class="pink"></div>
</div>
</body>
</html>`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                            <ExerciseGroup task={"Responsiv mit Grid Mobile-First"} taskNumber={"3.2"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Spielen Sie zunächst das Grid Garden -Spiel, um Grid Layout zu lernen. Implementieren Sie dann das gleiche responsive Webdesign wie in Aufgabe 3.1 allerdings mit Grid und der Mobile-First-Strategie! Vermeiden Sie diesmal außerdem das Erscheinen von Scrollbars."
                                        }>
                                        <Code language={"html"}>
                                            {`<html>
<head>
  <style type="text/css">
    body, html {
    margin: 0;
  }
    .red {
    background-color: red;
    grid-row-start: 1;
    grid-row-end: 1;
    grid-column-start: 1;
    grid-column-end: span 5;
  }
    .green {
    background-color: greenyellow;
    grid-row-start: 2;
    grid-row-end: 2;
    grid-column-start: 1;
    grid-column-end: span 5;
  }
    .green, .pink {

  }
    .blue {
    background-color: blue;
    grid-row-start: 3;
    grid-row-end: 3;
    grid-column-start: 1;
    grid-column-end: span 5;
  }
    .pink {
    background-color: deeppink;
    grid-row-start: 4;
    grid-row-end: 4;
    grid-column-start: 1;
    grid-column-end: span 5;
  }
    .grid {
    display: grid;
    height: calc(160px + 160px + 160px + 1000px);
    grid-template-columns: 20% 20% 20% 20% 20%;
    grid-template-rows: 10% 10% 70% 10%;
  }
    @media (min-width: 768px) {
    .grid {
    grid-template-rows: 10% 80% 10%;
    grid-template-columns: calc(20% - 10px) calc(20% - 10px) calc(20% - 10px) calc(20% - 10px) calc(20% - 10px);
    grid-gap: 10px;
    margin: 10px;
    width: calc(100% - 10px);
    box-sizing: border-box;
  }
    .green {
    grid-area: 2 / 1 / 2 / 2;
  }
    .blue {
    grid-area: 2 / 2 / 2 / 6;
  }
    .red {
    grid-area: 1 / 1 / 1 / 6;
  }
    .pink {
    grid-area: 3 / 1 / 3 / 6;
  }
  }
    @media (min-width: 992px) {
    .green {
    grid-area: 2 / 1 / 2 / 1;
  }
    .grid {
    grid-template-rows: 15% 85%;
  }
    .blue {
    grid-area: 2 / 2 / 2 / 5;
  }
    .pink {
    grid-area: 2 / 5 / 2 / 6;
  }
  }
  </style>
</head>
<body>
<div class="grid">
  <div class="red"></div>
  <div class="green"></div>
  <div class="blue"></div>
  <div class="pink"></div>
</div>
</body>
</html>`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div className={"mt-5"}>
                            <ExerciseGroup task={"Responsiv mit Grid"} taskNumber={"3.3"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Implementieren Sie folgende Landing Page responsiv mit Grid Layout. Vermeiden Sie außerdem das Erscheinen von Scrollbars so weit wie möglich."
                                        }>
                                        <Code language={"html"}>
                                            {`<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
    <script type="application/javascript">
      document.addEventListener("DOMContentLoaded", () => {
        const menuButton = document.getElementById("menu-button");
        const menuButtonClose = document.getElementById("menu-button-close");
        const sideNav = document.getElementById("sidenav");

        menuButton.addEventListener("click", () => {
          sideNav.classList.add("open")
        })
        menuButtonClose.addEventListener("click", () => {
          sideNav.classList.remove("open")
        })
      })
    </script>

    <style type="text/css">
        body, html {
            margin: 0;
            font-family: Roboto, sans-serif;
        }
        #menu-button {
            display: none;
        }
      nav {
          background-color: #363636;
          color: white;
          grid-row: 1 / 1;
          display: flex;
          justify-content: center;
          align-items: center;
      }
      nav a {
          margin: 0 15px;
      }
      h1 {
          text-align: center;
      }
      .grid {
          display: grid;
          height: 100vh;
          grid-template-columns: 100%;
          grid-template-rows: 7.5% 72.5% 20%;
      }
      .content-area {
          display: flex;
          justify-content: space-between;
          align-items: center;
      }
      .image-wrapper {
          width: 50%;
          display: flex;
          justify-content: center;
          align-items: center;
      }
      .content-area img {
          width: 100%;
          display: block;
          max-width: 350px;
          border: 1px solid black;
          border-radius: 10px;
      }
      main {
          grid-row: 2 / 2;
          background-color: #EBEAE5;
      }
      .text {
          display: flex;
          flex-direction: column;
          align-items: center;
          flex: 1;
      }
      .text p {
          margin-bottom: 50px;
          text-align: center;
      }
      footer {
          grid-row: 3 / 3;
          background-color: black;
          color: white;
          display: flex;
          justify-content: center;
          align-items: center;
          flex-direction: column;
      }
      button {
          font-family: 'Oswald', sans-serif;;
          border: none;
          background-color: #F88E37;
          color: white;
          padding: 5px 10px;
          font-size: 18px;
          width: 50%;
          border-radius: 5px;
      }
      #sidenav {
          display: none;
      }
      @media all and (max-width: 768px) {
          .content-area {
              flex-direction: column;
          }
          .content-area img {
              width: 200px;
              margin-bottom: 30px;
          }
          .content-area p {
              margin-bottom: 10px;
          }
         .content-area button {
             margin-top: 20px;
         }
      }
      @media all and (max-width: 576px) {
          #menu-button {
              display: block;
              padding-left: 10px;
          }
          #sidenav {
              display: block;
              position: fixed;
              width: 75%;
              top: 0;
              bottom: 0;
              z-index: 100;
              left: -75%;
              transition: all 0.5s;
          }
          #sidenav.open {
              left: 0;
          }
          #menu-button-close {
              position: absolute;
              top: 0;
              right: 0;
              padding: 10px;
          }
          #sidenav nav {
              flex-direction: column;
              height: 100%;
              justify-content: space-evenly;
          }
           button {
              margin-bottom: 20px;
          }
          body, html {
              overflow: hidden;
          }
          footer button {
              width: 75%;
          }
          footer {
              text-align: center;
          }
        nav.header-nav a {
            white-space: nowrap;
            text-overflow: ellipsis;
            width: 25%;
            overflow:hidden;
            display: none;
        }
        nav {
            justify-content: flex-start;
        }
          main {
              overflow: scroll;
          }
      }
    </style>
  </head>
  <body>
  <div id="sidenav">
    <nav>
      <div id="menu-button-close">
        X
      </div>
      <a>The book series</a>
      <a>Testimonials</a>
      <a>The Author</a>
      <a>Free resources</a>
    </nav>
  </div>
  <div class="grid">
    <nav class="header-nav">
      <div id="menu-button">
        Menü
      </div>
      <a>The book series</a>
      <a>Testimonials</a>
      <a>The Author</a>
      <a>Free resources</a>
    </nav>
    <main>
      <h1>You dont know JavaScript</h1>
      <div class="content-area">
        <div class="image-wrapper">
          <img alt="Complete JS Series" src="https://kaul.inf.h-brs.de/we/assets/img/landing-img.png" />
        </div>
      <div class="text">
        <p>Don't just drift through javascript.</p>
        <p>Understand how javascript works</p>
        <p>Start your journey through the bumpy side of javascript</p>
        <button>Order your copy now</button>
      </div>
      </div>
    </main>
    <footer>
      <p>The first ebook in the book series is absolutely free.</p>
      <button>Find out more about this offer</button>
    </footer>
  </div>
  </body>
</html>
`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                    </div>
                </div>
            </div>
        </Page>
    );
};

export default U3;
