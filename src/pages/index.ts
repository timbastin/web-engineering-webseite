import Home from "./home";
import U1 from "./u1";
import U2 from "./u2";
import U3 from "./u3";
import U4 from "./u4";
import U5 from "./u5";
import { ReactNode } from "react";
import U6 from "./u6";
import U7 from "./u7";
import U8 from "./u8";
import Imprint from "./Imprint";
import U9 from "./u9";
import U10 from "./u10";

interface BaseRoute {
    title: string;
    path: string;
}
export type AppRoute = LeafRoute | ParentRoute;

interface ParentRoute extends BaseRoute {
    children: AppRoute[];
}

interface LeafRoute extends BaseRoute {
    Component: () => ReactNode;
}

const routes: AppRoute[] = [
    {
        title: "Startseite",
        Component: Home,
        path: "",
    },
    {
        title: "Übungen",
        path: "uebungen",
        children: [
            {
                title: "Übung 1",
                path: "uebung1",
                Component: U1,
            },
            {
                title: "Übung 2",
                path: "uebung2",
                Component: U2,
            },
            {
                title: "Übung 3",
                path: "uebung3",
                Component: U3,
            },
            {
                title: "Übung 4",
                path: "uebung4",
                Component: U4,
            },
            {
                title: "Übung 5",
                path: "uebung5",
                Component: U5,
            },
            {
                title: "Übung 6",
                path: "uebung6",
                Component: U6,
            },
            {
                title: "Übung 7",
                path: "uebung7",
                Component: U7,
            },
            {
                title: "Übung 8",
                path: "uebung8",
                Component: U8,
            },
            {
                title: "Übung 9",
                path: "uebung9",
                Component: U9,
            },
            {
                title: "Übung 10",
                path: "uebung10",
                Component: U10,
            },
        ],
    },
    {
        title: "Impressum",
        Component: Imprint,
        path: "impressum",
    },
];

export default routes;
