import React from "react";
import Page from "../components/Page";
import Exercise from "../components/Exercise";
import ExerciseGroup from "../components/ExerciseGroup";
import Sidebar from "../components/Sidebar";
import Code from "../components/Code";

const links = [
    {
        anchor: "10.1",
        title: "Registrierung mit PHP",
    },
    {
        anchor: "10.2",
        title: "Login mit PHP",
    },
    {
        anchor: "10.3",
        title: "WWW-Navigator zum Content-Editor ausbauen",
    },
];
const U10 = () => {
    return (
        <Page title={"Übung 10 - PHP"}>
            <div className={"container"}>
                <div className={"row"}>
                    <div className={"col-md-3"}>
                        <Sidebar links={links} />
                    </div>
                    <div className={"col-md-9"}>
                        <div>
                            <ExerciseGroup task={"Registrierung mit PHP"} taskNumber={"10.1"}>
                                <p>
                                    Erstellen Sie mit PHP 5 auf www2.inf.h-brs.de ein Registrierungsformular. Speichern
                                    Sie die eingegebenen Daten persistent in einer Datei auf www2.inf.h-brs.de.
                                    Schreiben Sie Ihre PHP-Scripte so, dass es zu keinen Nebenläufigkeitsartefakten
                                    (z.B. Lost Update) kommen kann, egal wie viele Nutzer sich gleichzeitig
                                    registrieren.
                                </p>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Geben Sie hier Ihre vollständige Lösung für das Registrierungsformular (ggfs. mit allen Dateien hintereinander) ein:"
                                        }>
                                        <Code noIframe={true} language={"php"}>{`index.php

<?php

require_once("./cors.php");

class UserService {
    static private $SALT = "c29voNHiNAZjrhX!qR6P";
    private $data;
    function __construct() {
        $this->data = json_decode(file_get_contents("./user.json"), true);
    }
    public function check_if_registered($email) {
        return $this->get_user($email) !== null;
    }

    private function get_user($email) {
        foreach ($this->data as $e => $password) {
            if ($e === $email) {
                return [
                    "email" => $e,
                    "password" => $password,
                ];
            }
        }
        return null;
    }

    public function register_user($email, $password) {
        $this->data[$email] = hash("sha256", $password . self::$SALT);
        $this->updateFile();
    }

    private function updateFile() {
        file_put_contents("./user.json", json_encode($this->data));
    }

    public function verify($email, $password) {
        $user = $this->get_user($email);
        if (!$user) {
            return false;
        }
        return hash_equals($user["password"], $password);
    }
}

cors();
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE);
if (!$input["email"] || !$input["password"]) {
    http_response_code(400);
    return;
}

$controller = new UserService();

function register() {
    global $controller;
    global $input;
    // check if the user is already registered.
    if ($controller->check_if_registered($input["email"])) {
        http_response_code(409);
        return [
            "success" => false
        ];
    }

    // we can add the user.
    $controller->register_user($input["email"], $input["password"]);
    return [
        "success" => true
    ];
}


switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        return register();
}

cors.php

<?php

function cors() {
    // Allow from any origin
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
        // you want to allow, and if so:
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            // may also be using PUT, PATCH, HEAD etc
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
    }
}

index.html

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>10.1</title>
  <style>
    label {
        display: block;
    }
    div {
        margin-bottom: 10px;
    }
  </style>
</head>
<body>
<h2>Registrieren</h2>
  <form>
    <div>
      <label for="email">E-Mail Adresse eingeben</label>
      <input type="email" id="email" />
    </div>
    <div>
      <label for="password">Passwort eingeben</label>
      <input type="password" id="password" />
    </div>
    <div>
      <label for="passwordRepeat">Passwort wiederholen</label>
      <input type="password" id="passwordRepeat" />
    </div>

    <button id="form-submit" type="button">Registrieren</button>
  </form>
  <script>
    function getFormValues() {
      return ["email", "password", "passwordRepeat"].map(id => {
        return document.getElementById(id).value
      });
    }
    async function handleSubmit(e) {
        e.preventDefault();
        const [email, password, passwordRepeat] = getFormValues();
        if (password !== passwordRepeat) {
          alert("Beide Passwörter stimmen nicht überein.");
          return;
        }

        const res = await fetch("http://localhost/index.php", {
          method: "POST",
          mode: "cors",
          body: JSON.stringify({email, password})
        });
        console.log(await res.json());
        if (res.status === 201) {
          alert("Erfolgreich registriert");
        }
    }
    const f = document.getElementById("form-submit");
    f.addEventListener("click", handleSubmit)
  </script>
</body>
</html>



`}</Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div className={"mt-5"}>
                            <ExerciseGroup task={"Login mit PHP"} taskNumber={"10.2"}>
                                <p>
                                    Schreiben Sie eine sichere PHP-Lösung für Login, das die persistierten
                                    Registrierungsdaten aus der letzten Aufgabe nutzt.
                                </p>
                                <div className={"mb-4"}>
                                    <Exercise task={"Geben Sie alle Ihre PHP-Skripte hier an:"}>
                                        <Code noIframe={true} language={"php"}>
                                            {`<?php

require_once("./cors.php");

class UserService {
    private $data;
    function __construct() {
        $this->data = json_decode(file_get_contents("./user.json"), true);
    }
    public function check_if_registered($email) {
        return $this->get_user($email) !== null;
    }

    private function get_user($email) {
        foreach ($this->data as $e => $password) {
            if ($e === $email) {
                return [
                    "email" => $e,
                    "password" => $password,
                ];
            }
        }
        return null;
    }

    public function register_user($email, $password) {
        $this->data[$email] = password_hash($password, PASSWORD_BCRYPT);
        $this->updateFile();
    }

    private function updateFile() {
        file_put_contents("./user.json", json_encode($this->data));
    }

    public function verify($email, $password) {
        $user = $this->get_user($email);
        if (!$user) {
            return false;
        }

        return password_verify($password, $user["password"]);
    }
}

cors();
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE);
if (!$input["email"] || !$input["password"]) {
    http_response_code(400);
    return;
}

$controller = new UserService();

function register() {
    global $controller;
    global $input;
    // check if the user is already registered.
    if ($controller->check_if_registered($input["email"])) {
        http_response_code(409);
        return [
            "success" => false
        ];
    }

    // we can add the user.
    $controller->register_user($input["email"], $input["password"]);
    return [
        "success" => true
    ];
}

function login() {
    global $controller;
    global $input;

    if (!$controller->verify($input["email"], $input["password"])) {
        http_response_code(403);
        return [
            "success" => false
        ];
    }
    return [
        "success" => true
    ];
}


switch ($input["action"]) {
    case 'register':
        echo json_encode(register());
        return;
    case "login":
        echo json_encode(login());
}

<?php

function cors() {
    // Allow from any origin
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
        // you want to allow, and if so:
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            // may also be using PUT, PATCH, HEAD etc
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
    }
}

user.json
{"test@email.com":"$2y$10$ttiykVnGuVNIOw0MXUwFhuwRNakSDt40.X0lF7dAe2Tb86qbrJV4e"}
`}
                                        </Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Geben Sie Ihren Client-Code (HTML, CSS, Javascript), falls zusätzlich zum PHP-Skript vorhanden, hier an:"
                                        }>
                                        <Code noIframe={true} language={"php"}>
                                            {`<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>10.1</title>
  <style>
    label {
        display: block;
    }
    div {
        margin-bottom: 10px;
    }
  </style>
</head>
<body>
<h2>Registrieren</h2>
  <form>
    <div>
      <label for="email">E-Mail Adresse eingeben</label>
      <input type="email" id="email" />
    </div>
    <div>
      <label for="password">Passwort eingeben</label>
      <input type="password" id="password" />
    </div>
    <div>
      <label for="passwordRepeat">Passwort wiederholen</label>
      <input type="password" id="passwordRepeat" />
    </div>

    <button id="form-submit" type="button">Registrieren</button>
  </form>

<h2>Login</h2>
<form>
  <div>
    <label for="email-login">E-Mail Adresse eingeben</label>
    <input type="email" id="email-login" />
  </div>
  <div>
    <label for="password-login">Passwort eingeben</label>
    <input type="password" id="password-login" />
  </div>
  <button id="form-submit-login" type="button">Einloggen</button>
</form>
  <script>
    function getRegisterFormValues() {
      return ["email", "password", "passwordRepeat"].map(id => {
        return document.getElementById(id).value
      });
    }

    function getLoginFormValues() {
      return ["email-login", "password-login"].map(id => {
        return document.getElementById(id).value
      });
    }
    async function handleRegister(e) {
        e.preventDefault();
        const [email, password, passwordRepeat] = getRegisterFormValues();
        if (password !== passwordRepeat) {
          alert("Beide Passwörter stimmen nicht überein.");
          return;
        }

        const res = await fetch("http://localhost/index.php", {
          method: "POST",
          mode: "cors",
          body: JSON.stringify({email, password, action: "register"})
        });
        if (res.status === 20) {
          alert("Erfolgreich registriert");
        } else if(res.status === 409) {
          alert("Diese E-Mail ist bereits registriert");
        }
    }
    async function handleLogin(e) {
      e.preventDefault();
      const [email, password] = getLoginFormValues();

      const res = await fetch("http://localhost/index.php", {
        method: "POST",
        mode: "cors",
        body: JSON.stringify({email, password, action: "login"})
      });
      if (res.status === 200) {
        alert("Erfolgreich eingeloggt");
      } else if(res.status === 403) {
        alert("Login nicht möglich. Bitte überprüfe dein Passwort und deine E-Mail Adresse.");
      }
    }
    const registerButton = document.getElementById("form-submit");
    registerButton.addEventListener("click", handleRegister)

    const loginButton = document.getElementById("form-submit-login");
    loginButton.addEventListener("click", handleLogin)
  </script>
</body>
</html>
`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div className={"mt-5"}>
                            <ExerciseGroup task={"WWW-Navigator zum Content-Editor ausbauen"} taskNumber={"10.3"}>
                                <p>
                                    Bauen Sie Ihren WWW-Navigator zum Content-Editor aus, mit dem Sie weitere Inhalte
                                    hinzu fügen können, die persistent auf dem Server mittels PHP gespeichert werden.
                                    Schreiben Sie Ihre PHP-Scripte so, dass es zu keinen Nebenläufigkeitsartefakten
                                    kommen kann, egal wie viele Nutzer gleichzeitig editieren und speichern.
                                </p>
                                <p>
                                    Speichern Sie die Inhalte Ihres WWW-Navigators auf dem Server www2.inf.h-brs.de.
                                    Erweitern Sie Ihren WWW-Navigator um eine Edit-Funktionalität, so dass Inhalte
                                    editiert und ergänzt werden können. Vermeiden Sie die Lost Update-Anomalie.
                                </p>
                                <p>Schützen Sie Ihre Inhalte mit einem Login.</p>
                                <div className={"mb-4"}>
                                    <Exercise task={"Geben Sie alle Ihre PHP-Skripte hier an:"}>
                                        <Code noIframe={true} language={"php"}>{`
<?php

require_once("./cors.php");
cors();

$data = json_decode(file_get_contents("./www-navigator-content.json"), true);
switch($_SERVER["REQUEST_METHOD"]){
    case 'POST':
        // update the value
        $inputJSON = file_get_contents('php://input');
        $input = json_decode($inputJSON, TRUE);
        $data[$input["key"]][$input["category"]]["content"] = $input["content"];
        file_put_contents("./www-navigator-content.json", json_encode($data));
        echo [
            "success" => true,
        ];
        break;
    case "GET":
        // retrieve the data.
        echo json_encode($data[$_GET['data']]);
}
`}</Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Geben Sie alle Ihre Ajax- oder Fetch-Skripte, falls zusätzlich zum PHP-Skript vorhanden, hier an:"
                                        }>
                                        <Code noIframe={true} language={"javascript"}>
                                            {`
export const fetchData = async (fileName) => {
  const response = await fetch("http://localhost/www-navigator.php?data=" + fileName)
  return response.json()
}

export default async function updateData(key, category, content) {
  const response = await fetch("http://localhost/www-navigator.php", {
    method: "POST",
    body: JSON.stringify({
      key,
      category,
      content,
    })
  });
  return response.json()
}
`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                    </div>
                </div>
            </div>
        </Page>
    );
};

export default U10;
