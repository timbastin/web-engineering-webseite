import React from "react";
import Page from "../components/Page";
import Exercise from "../components/Exercise";
import ExerciseGroup from "../components/ExerciseGroup";
import Sidebar from "../components/Sidebar";
import Code from "../components/Code";

const links = [
    {
        anchor: "9.1",
        title: "Komponente in Vue.js",
    },
    {
        anchor: "9.2",
        title: "Menü-Komponente",
    },
    {
        anchor: "9.3",
        title: "Vue.js WWW-Navigator",
    },
];
const U9 = () => {
    return (
        <Page title={"Übung 9 - Vue.js"}>
            <div className={"container"}>
                <div className={"row"}>
                    <div className={"col-md-3"}>
                        <Sidebar links={links} />
                    </div>
                    <div className={"col-md-9"}>
                        <div>
                            <ExerciseGroup task={"Komponente in Vue.js"} taskNumber={"9.1"}>
                                <p>
                                    Schreiben Sie eine Vue.js Single File Component mit einem Text-Eingabefeld und 3
                                    Ausgabefeldern, in denen man während des Tippens sehen kann, (a) wie viele
                                    Buchstaben (b) wie viele Leerzeichen und (c) wie viele Worte man in das
                                    Text-Eingabefeld bereits eingegeben hat.
                                </p>
                                <p>
                                    Betten Sie Ihre Komponente in eine Webseite zweimal ein und testen Sie, ob beide
                                    Komponenten unabhängig voneinander sind.
                                </p>
                                <div className={"mb-4"}>
                                    <Exercise task={"Geben Sie Ihre Vue.js Single File Component hier ein:"}>
                                        <Code noIframe={true} language={"vue"}>{`<template>
  <input v-model="message" />
  <p>
    Anzahl Buchstaben: {{letters}}
  </p>
  <p>
    Anzahl Leerzeichen: {{whitespaces}}
  </p>
  <p>
    Anzahl Worte: {{words}}
  </p>
</template>

<script>
export default {
  name: 'HelloWorld',
  data() {
    return {
      message: "",
    }
  },
  computed: {
    words() {
      const arr = this.message.split(" ");
      return arr.filter(el => el !== " " && el !== "").length
    },
    whitespaces() {
      return [...this.message].filter(l => l === " ").length
    },
    letters() {
      return this.message.length
    },
  },
  props: {
    msg: String
  }
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>
h3 {
  margin: 40px 0 0;
}
ul {
  list-style-type: none;
  padding: 0;
}
li {
  display: inline-block;
  margin: 0 10px;
}
a {
  color: #42b983;
}
</style>

`}</Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Geben Sie die Webseite, auf der Sie Ihre Komponente mehrfach testen, hier ein:"
                                        }>
                                        <Code noIframe={true} language={"vue"}>{`<template>
  <Eingabe/>
  <Eingabe/>
  <Eingabe/>
</template>

<script>
import Eingabe from './components/Eingabe.vue'

export default {
  name: 'App',
  components: {
    Eingabe
  }
}
</script>

<style>
#app {
  font-family: Avenir, Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: #2c3e50;
  margin-top: 60px;
}
</style>

index.html:
<!DOCTYPE html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link rel="icon" href="<%= BASE_URL %>favicon.ico">
    <title><%= htmlWebpackPlugin.options.title %></title>
  </head>
  <body>
    <noscript>
      <strong>We're sorry but <%= htmlWebpackPlugin.options.title %> doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
    </noscript>
    <div id="app"></div>
    <!-- built files will be auto injected -->
  </body>
</html>
`}</Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div className={"mt-5"}>
                            <ExerciseGroup task={"Menü-Komponente"} taskNumber={"9.2"}>
                                <p>
                                    Schreiben Sie eine möglichst flexible Vue.js Single File Component für Menüs und
                                    wenden Sie diese in derselben Webseite zweimal an, einmal horizontal, das andere Mal
                                    vertikal.
                                </p>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Geben Sie die Inhalte aller Dateien Ihrer Lösung inkl. JS-Quelltext hintereinander ein. Schreiben Sie vor jede Datei deren Dateiname:"
                                        }>
                                        <Code noIframe={true} language={"vue"}>
                                            {`Menu.vue:

<template>
  <div :class="mode" class="menu">
    <div class="menu-item" :key="link.href" v-for="link in links">
      <a :href="link.href">{{link.title}}</a>
    </div>
  </div>
</template>

<script>
export default {
  name: 'Menu',
  data() {
    return {
      message: "",
    }
  },
  props: {
    mode: {
      type: String,
      default: "horizontal"
    },
    links: {
      type: Array,
      default: () => [],
    }
  }
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>
  .menu {

  }
  .menu.horizontal {
    display: flex;
    justify-content: space-between;
  }
  .menu-item {
    background-color: #eeee;
    flex-grow: 1;
    padding: 5px 10px;
  }
  a {
    text-decoration: none;
  }
</style>

App.vue:

<template>
  <Menu mode="horizontal" v-bind:links="links" />
  <Menu mode="vertical" v-bind:links="links" />
</template>

<script>
import Menu from "@/components/Menu";

export default {
  name: 'App',
  components: {
    Menu
  },
  data() {
    return {
      links: [
        {
          href: "https://google.com",
          title: "Google"
        },
        {
          href: "https://l3montree.com",
          title: "L3montree"
        },
        {
          href: "https://stamplab.de",
          title: "StampLab"
        }
      ]
    }
  }
}
</script>

<style>
#app {
  font-family: Avenir, Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: #2c3e50;
  margin-top: 60px;
}
</style>


index.html:
<!DOCTYPE html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link rel="icon" href="<%= BASE_URL %>favicon.ico">
    <title><%= htmlWebpackPlugin.options.title %></title>
  </head>
  <body>
    <noscript>
      <strong>We're sorry but <%= htmlWebpackPlugin.options.title %> doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
    </noscript>
    <div id="app"></div>
    <!-- built files will be auto injected -->
  </body>
</html>
`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div className={"mt-5"}>
                            <ExerciseGroup task={"Vue.js WWW-Navigator"} taskNumber={"9.3"}>
                                <p>
                                    Schreiben Sie Ihren WWW-Navigator als SPA in Vue.js mit Vue Router und/oder mit Vuex
                                    als State Manager.
                                </p>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Dokumentieren Sie Ihren Entscheidungsprozess: In welche Komponenten wollen Sie Ihre App zerlegen? Wie soll das State Management implementiert werden?"
                                        }>
                                        <p>Die App wurde in Folgende Komponenten zerlegt:</p>
                                        <ol>
                                            <li>Page (Die Struktur der Seite)</li>
                                            <li>Main (Der eigentliche Content)</li>
                                            <li>Navigation (Die Navigation)</li>
                                            <li>Reference (Die rechte Sidebar)</li>
                                            <li>Sidebar (Die linke Sidebar)</li>
                                            <li>Subpage (Das Layout des Contents, und den beiden Sidebars)</li>
                                            <li>Title (Der Titel der Seite)</li>
                                        </ol>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Dokumentieren Sie Ihren Entscheidungsprozess: In welche Komponenten wollen Sie Ihre App zerlegen? Wie soll das State Management implementiert werden?"
                                        }>
                                        <Code noIframe={true} language={"html"}>
                                            {`
index.html:
<!DOCTYPE html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link rel="icon" href="<%= BASE_URL %>favicon.ico">
    <title><%= htmlWebpackPlugin.options.title %></title>
  </head>
  <body>
    <noscript>
    <strong>We're sorry but <%= htmlWebpackPlugin.options.title %> doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
      </noscript>
      <div id="app"></div>
      <!-- built files will be auto injected -->
  </body>
</html>

App.vue:
<template>
  <Page>
    <template v-slot:header>
      <Title title="WWW-Navigator" />
      <Navigation v-bind:links="links" />
    </template>
    <template v-slot:main>
      <router-view/>
    </template>
  </Page>
</template>

<style>
#app {
  font-family: Avenir, Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: #2c3e50;
}

#nav {
  padding: 30px;
}

#nav a {
  font-weight: bold;
  color: #2c3e50;
}

#nav a.router-link-exact-active {
  color: #42b983;
}
</style>
<script>
import Page from "@/components/Page";
import Navigation from "@/components/Navigation";
import Title from "@/components/Title";
import store from "./store";
export const links =  [
  {
    href: "/html",
    title: "HTML"
  },
  {
    href: "/css",
    title: "CSS"
  },
  {
    href: "/javascript",
    title: "JavaScript"
  }
]
export default {
  components: { Page, Navigation, Title },
  store,
  mounted() {
    store.dispatch("fetchData", "html");
  },
  data() {
    return {
      links,
    }
  }
}
</script>

CSS.vue:
<template>
  <SubPage>
    <template v-slot:sidebar>
      <Sidebar :on-click="handleClick" :links="links" />
    </template>
    <template v-slot:content>
      <Main :content="this.content" />
    </template>
    <template v-slot:references>
      <Reference :reference="reference" />
    </template>
  </SubPage>
</template>

<script>
import SubPage from "@/components/SubPage";
import Sidebar from "@/components/Sidebar";
import { fetchData } from "@/services/fetchData";
import Reference from "@/components/Reference";
import Main from "@/components/Main";
export default {
  name: 'CSS',
  components: { Reference, SubPage, Sidebar, Main },
  data() {
    return {
      data: {},
      content: "",
      reference: "",
      links: [],
    }
  },
  methods: {
    handleClick(link) {
      this.content = this.data[link].content;
      this.reference = this.data[link].references[0];
      this.links = Object.keys(this.data).map((item) => {
        return {
          title: item,
          active: link === item,
        }
      })
    }
  },
  async mounted () {
    const res = await fetchData("css");
    this.data = res;
    this.links = Object.keys(res).map((item, index) => {
      return {
        title: item,
        active: index === 0,
      }
    })
    this.content = res[this.links[0].title].content;
    this.reference = res[this.links[0].title].references[0];
    console.log(this.content, this.reference);
  }
}
</script>

HTML.vue:
<template>
  <SubPage>
    <template v-slot:sidebar>
      <Sidebar :on-click="handleClick" :links="links" />
    </template>
    <template v-slot:content>
      <Main :content="this.content" />
    </template>
    <template v-slot:references>
      <Reference :reference="reference" />
    </template>
  </SubPage>
</template>

<script>
import SubPage from "@/components/SubPage";
import Sidebar from "@/components/Sidebar";
import { fetchData } from "@/services/fetchData";
import Reference from "@/components/Reference";
import Main from "@/components/Main";
export default {
  name: 'HTML',
  components: { Reference, SubPage, Sidebar, Main },
  data() {
    return {
      data: {},
      content: "",
      reference: "",
      links: [],
    }
  },
  methods: {
    handleClick(link) {
      this.content = this.data[link].content;
      this.reference = this.data[link].references[0];
      this.links = Object.keys(this.data).map((item) => {
        return {
          title: item,
          active: link === item,
        }
      })
    }
  },
  async mounted () {
    console.log(this.filename)
    const res = await fetchData("html");
    this.data = res;
    this.links = Object.keys(res).map((item, index) => {
      return {
        title: item,
        active: index === 0,
      }
    })
    this.content = res[this.links[0].title].content;
    this.reference = res[this.links[0].title].references[0];
  }
}
</script>

JavaScript.vue:
<template>
  <SubPage>
    <template v-slot:sidebar>
      <Sidebar :on-click="handleClick" :links="links" />
    </template>
    <template v-slot:content>
      <Main :content="this.content" />
    </template>
    <template v-slot:references>
      <Reference :reference="reference" />
    </template>
  </SubPage>
</template>

<script>
import SubPage from "@/components/SubPage";
import Sidebar from "@/components/Sidebar";
import { fetchData } from "@/services/fetchData";
import Reference from "@/components/Reference";
import Main from "@/components/Main";
export default {
  name: 'JavaScript',
  components: { Reference, SubPage, Sidebar, Main },
  data() {
    return {
      data: {},
      content: "",
      reference: "",
      links: [],
    }
  },
  methods: {
    handleClick(link) {
      this.content = this.data[link].content;
      this.reference = this.data[link].references[0];
      this.links = Object.keys(this.data).map((item) => {
        return {
          title: item,
          active: link === item,
        }
      })
    }
  },
  async mounted () {
    const res = await fetchData("javascript");
    this.data = res;
    this.links = Object.keys(res).map((item, index) => {
      return {
        title: item,
        active: index === 0,
      }
    })
    this.content = res[this.links[0].title].content;
    this.reference = res[this.links[0].title].references[0];
    console.log(this.content, this.reference);
  }
}
</script>
Main.vue:
<template>
  <div>
    <h3>Erklärung:</h3>
    {{content}}
  </div>
</template>

<script>
export default {
  name: 'Main',
  props: {
    content: String
  }
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>

</style>
Navigation.vue:
<template>
  <div class="navigation">
    <div v-for="link in links" :key="link.key">
      <router-link
        :to="link.href"
        >
        {{ link.title }}
      </router-link>
    </div>
  </div>
</template>

<script>

export default {
  name: 'Navigation',
  props: {
    links: Array,
    default: () => [],
  }
}
</script>

<style scoped>
.navigation {
  display: flex;
}
.navigation > div {
  padding: 5px 10px;
}

</style>
Page.vue:
<template>
  <div id="page">
    <header>
      <slot name="header" />
    </header>
    <div id="content">
      <main>
        <slot name="main"/>
      </main>
    </div>
  </div>
</template>

<script>
export default {
  name: 'Page',
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>

</style>
Reference.vue:
<template>
  <div>
    <h3>Quellen:</h3>
    <a :href="reference">{{reference}}</a>
  </div>
</template>

<script>
export default {
  name: 'Reference',
  props: {
    reference: String,
  }
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>
  a {
    word-break: break-all;
    text-align: left!important;
  }
</style>
Sidebar.vue:
<template>
  <div>
    <h3>Navigation</h3>
    <span @click="onClick(link.title)" :key="link.title" v-for="link in links">{{link.title}}</span>
  </div>
</template>

<script>
export default {
  name: 'Sidebar',
  props: {
    links: Array,
    onClick: Function,
  }
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>
  span {
    display: block;
    cursor: pointer;
  }
</style>
Subpage.vue:
<template>
      <div id="subpage">
        <div class="sidebar">
          <slot name="sidebar"/>
        </div>
        <div class="content">
        <slot name="content"/>
        </div>
        <div class="references">
          <slot name="references"/>
        </div>
      </div>
</template>

<script>
export default {
  name: 'SubPage',
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>
  #subpage {
    display: flex;
  }
  .sidebar, .references  {
    flex: 1;
    padding: 10px;
    text-align: left;
  }
  .references {
    text-align: left!important;
  }
  .content {
    padding: 10px;
    flex: 3;
    text-align: left;
  }

</style>
Title.vue:
<template>
  <h1>{{title}}</h1>
</template>

<script>
export default {
  name: 'Title',
  props: {
    title: String,
  }
}
</script>

<style>

</style>
`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                    </div>
                </div>
            </div>
        </Page>
    );
};

export default U9;
