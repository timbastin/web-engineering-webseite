import React from "react";
import Page from "../components/Page";
import Exercise from "../components/Exercise";
import ExerciseGroup from "../components/ExerciseGroup";
import Sidebar from "../components/Sidebar";
import Code from "../components/Code";

const links = [
    {
        anchor: "2.1",
        title: "CSS Selektoren und CSS Farben",
    },
    {
        anchor: "2.2",
        title: "CSS Positionierung",
    },
    {
        anchor: "2.3",
        title: "Wireframe with HTML and CSS",
    },
];
const U2 = () => {
    return (
        <Page title={"Übung 2 - CSS 1. Teil"}>
            <div className={"container"}>
                <div className={"row"}>
                    <div className={"col-md-3"}>
                        <Sidebar links={links} />
                    </div>
                    <div className={"col-md-9"}>
                        <div>
                            <ExerciseGroup
                                task={"Fachliche Argumentation über Erfolgsprinzipien des WWW"}
                                taskNumber={"2.1"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={`Berechnen Sie die Spezifität folgender CSS-Selektoren:
div div div:focus  .inner
h1 + div.main
div a:link[href*='h-brs']
nav > a:hover::before
ul#primary-nav  li.active`}>
                                        <ol>
                                            <li>23</li>
                                            <li>12</li>
                                            <li>22</li>
                                            <li>13</li>
                                            <li>112</li>
                                        </ol>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Rechnen Sie folgende RGB-Werte in HSL-Werte um:\n" +
                                            "\n" +
                                            "#ffffff\n" +
                                            "#000\n" +
                                            "#ab0978\n" +
                                            "rgb(127,255,33)\n" +
                                            "rgba(255,127,33,0.8)"
                                        }>
                                        <ol>
                                            <li>hsl(0,0%,100%)</li>
                                            <li>hsl(0,0%,100%)</li>
                                            <li>hsl(319, 90%, 35%)</li>
                                            <li>hsl(95, 100%, 56%)</li>
                                            <li>hsla(25, 100%, 56%, 0.8)</li>
                                        </ol>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div className={"mt-5"}>
                            <ExerciseGroup task={"CSS Positionierung"} taskNumber={"2.2"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schauen Sie sich folgendes Video an und bauen Sie das dynamische Verhalten exakt nach (nur mit HTML und CSS, ohne JavaScript):"
                                        }>
                                        <Code language={"html"}>
                                            {`<html lang="de">
<head>
  <style type="text/css">
    main {
    width: 500px;
    max-height: 500px;
    overflow: scroll;
  }
    .headline {
    position: sticky;
    top: 0;
    background-color: white;
    padding: 5px 10px;
    border: 1px solid black;
  }
    h1 {
    margin: 0;
  }
  </style>
</head>
<body>
<main>
  <div class="headline">
    <h1>Erste Überschrift</h1>
  </div>
  <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
    eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
    voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
    clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
    amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
    nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed
    diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
    clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
    amet.</p>
  <div class="headline">
    <h1>Zweite Überschrift</h1>
  </div>
  <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
    eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
    voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
    clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
    amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
    nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed
    diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
    clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
    amet.</p>
  <div class="headline">
    <h1>Dritte Überschrift</h1>
  </div>
  <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
    eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
    voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
    clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
    amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
    nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed
    diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
    clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
    amet.</p>
  <div class="headline">
    <h1>Vierte Überschrift</h1>
  </div>
  <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
    eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
    voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
    clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
    amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
    nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed
    diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
    clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
    amet.</p>
</main>
</body>
</html>`}
                                        </Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schauen Sie sich folgendes Video an und bauen Sie das dynamische Verhalten exakt nach (nur mit HTML und CSS, ohne JavaScript):"
                                        }>
                                        <Code language={"html"}>
                                            {`<html lang="de">
<head>
  <style type="text/css">
    img {
    display: block;
    border: 1px solid black;
    padding: 5px;
  }
    input:checked ~ img {
    opacity: 0;
  }
  </style>
</head>
<body>
<main>
  <h1>Übung 2.2</h1>
  <div>
    <input id="checkbox" name="checkbox" type="checkbox">
      <label htmlFor="checkbox">hide and show via checkbox</label>
      <img width="500"
           src="https://upload.wikimedia.org/wikipedia/commons/a/a4/Hochschule_Bonn-Rhein-Sieg_Wolfgang_G%C3%B6ddertz_Induktion.jpg" />
  </div>
</main>
</body>
</html>`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div className={"mt-5"}>
                            <ExerciseGroup task={"Wireframe with HTML and CSS"} taskNumber={"2.3"}>
                                <div className={"mb-4"}>
                                    <Exercise task={"Wireframe nachbauen:"}>
                                        <Code language={"html"}>
                                            {`<html lang="de">
<head>
  <style type="text/css">
    body {
    background - color: #b2d6d1;
    font-family: Roboto,sans-serif;
  }
    .el {
    display: flex;
    width: 100%;
    margin: 15px 0;
  }
    h1 {
    text - align: center;
  }
    .content {
    margin: 20px;
    padding: 10px;
    background-color: #FAFAFA;
  }
    .content > p {
    text - align: center;
  }
    input {
    padding: 5px 10px;
  }
    .el > div {
    flex: 1;
    padding: 0px 5px;
  }
    .el >div:nth-child(1) {
    text - align: right;
  }
  </style>
</head>
<body>
<main>
  <h1>Survey Form</h1>
  <div className="content">
    <p>Let us know how we can improve freeCodeCamp</p>
    <div className="el">
      <div>*Name:</div>
      <div><input type="text" placeholder="Enter your name"></div>
    </div>
    <div className="el">
      <div>*Email:</div>
      <div><input type="text" placeholder="Enter your Email"></div>
    </div>
    <div className="el">
      <div>*Age:</div>
      <div><input type="number" placeholder="Age"></div>
    </div>
    <div className="el">
      <div>Which optiuon best describes your current role?</div>
      <div><select>
        <option value="student">Student</option>
      </select></div>
    </div>
    <div className="el">
      <div>How likely is that you would recommend freeCodeCamp to a friend?
      </div>
      <div className="radio-group">
        <div>
          <input id="a" name="recommend" type="radio" value="Definitely">
            <label htmlFor="a">Definitely</label>
        </div>
        <div>
          <input id="b" name="recommend" type="radio" value="Maybe">
            <label htmlFor="b">Maybe</label>
        </div>
        <div>
          <input id="c" name="recommend" type="radio" value="Not sure">
            <label htmlFor="c">Not sure</label>
        </div>
      </div>
    </div>
    <div className="el">
      <div>What you like the most in FCC?</div>
      <div><select>
        <option value="select-an-option">Select an option</option>
      </select></div>
    </div>
    <div className="el">
      <div>Things that should be improved in the future<br />(Check all that
        apply):
      </div>
      <div className="radio-group">
        <div>
          <input id="d" name="Front-end Projects" type="checkbox"
                 value="Front-end Projects">
            <label htmlFor="d">Front-end Projects</label>
        </div>
        <div>
          <input id="e" name="Back-end Projects" type="checkbox"
                 value="Back-end Projects">
            <label htmlFor="e">Back-end Projects</label>
        </div>
        <div>
          <input id="f" name="Data Visualization" type="checkbox"
                 value="Data Visualization">
            <label htmlFor="f">Data Visualization</label>
        </div>
      </div>
    </div>
  </div>
</main>
</body>
</html>`}
                                        </Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Was war in der Lösung dieser Aufgabe die größte Schwierigkeit und wie haben Sie diese gelöst?"
                                        }>
                                        Mir ist es sehr leicht gefallen.
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                    </div>
                </div>
            </div>
        </Page>
    );
};

export default U2;
