import React from "react";
import Page from "../components/Page";
import Exercise from "../components/Exercise";
import ExerciseGroup from "../components/ExerciseGroup";
import Sidebar from "../components/Sidebar";
import Code from "../components/Code";

const links = [
    {
        anchor: "4.1",
        title: "Funktionen",
    },
    {
        anchor: "4.2",
        title: "Objekte",
    },
    {
        anchor: "4.3",
        title: "Fibonacci",
    },
    {
        anchor: "4.4",
        title: "Topsort",
    },
];
const U4 = () => {
    return (
        <Page title={"Übung 4 - JavaScript"}>
            <div className={"container"}>
                <div className={"row"}>
                    <div className={"col-md-3"}>
                        <Sidebar links={links} />
                    </div>
                    <div className={"col-md-9"}>
                        <div>
                            <ExerciseGroup task={"Funktionen"} taskNumber={"4.1"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie eine Funktion identity(), die ein Argument als Parameter entgegen nimmt und dieses als Ergebnis zurück gibt."
                                        }>
                                        <Code noIframe={true} language={"javascript"}>
                                            {`function identity(param) {
    return param;
}`}
                                        </Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie eine Funktion identity_function(), die ein Argument als Parameter entgegen nimmt und eine Funktion zurück gibt, die dieses Argument zurück gibt."
                                        }>
                                        <Code noIframe={true} language={"javascript"}>
                                            {`function identity_function(param) {
    return function() {
        return param;
    };
}`}
                                        </Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie zwei binäre Funktionen add und mul, die Summe und Produkt berechnen."
                                        }>
                                        <Code noIframe={true} language={"javascript"}>
                                            {`function add (a, b)  {
    return a + b;
}
function mul(a, b)  {
    return a * b;
}`}
                                        </Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie eine Addier-Funktion addf(), so dass addf(x)(y) genau x + y zurück gibt. (Es haben also zwei Funktionsaufrufe zu erfolgen. addf(x) liefert eine Funktion, die auf y angewandt wird.)"
                                        }>
                                        <Code noIframe={true} language={"javascript"}>
                                            {`function addf(a) {
    return function(b) {
        return a + b;
    };
}`}
                                        </Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie eine Funktion applyf(), die aus einer binären Funktion wie add(x,y) eine Funktion addfberechnet, die mit zwei Aufrufen das gleiche Ergebnis liefert, z.B. addf = applyf(add); addf(x)(y) soll add(x,y) liefern. Entsprechend applyf(mul)(5)(6) soll 30 liefern, wenn mul die binäre Multiplikation ist."
                                        }>
                                        <Code noIframe={true} language={"javascript"}>
                                            {`function applyf(fun) {
    return function(a) {
        return function(b) {
            return fun(a, b);
        };
    };
}`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div className={"mt-5"}>
                            <ExerciseGroup task={"Objekte"} taskNumber={"4.2"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie die Prototypen Person und Auto in JavaScript, so dass jede Person weiß, welche Autos sie besitzt. Schreiben Sie eine Funktion conflict(), die feststellt, ob ein Auto von mehr als einer Person besessen wird."
                                        }>
                                        <Code noIframe={true} language={"javascript"}>
                                            {`function Person(autos) {
    this.autos = autos;
}

function Auto(kennzeichen) {
    this.kennzeichen = kennzeichen;
}

function conflict(personen) {
    var allIds = [];
    personen.forEach(person => {
        allIds.push(...person.autos.map(auto => auto.kennzeichen));
    });
    if (allIds.some((kennzeichen, index) => allIds.indexOf(kennzeichen) !== index)) {
        return true;
    }
}
var opel = new Auto("BN-AA-11");
var golf = new Auto("BN-AA-12");
var seb = new Person([opel, golf]);
var tim = new Person([opel]);
conflict([seb, tim]); // true
`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div>
                            <ExerciseGroup task={"Fibonacci"} taskNumber={"4.3"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Schreiben Sie im Browser die Fibonacci-Funktion in JavaScript und geben Sie die ersten 2000 Fibonacci-Zahlen 0,1,1,2,3,5,8,13,... auf der Konsole mit console.log() aus. Achten Sie auf Performanz: Berechnen Sie jeden Fibonacci-Wert nur einmal. Speichern Sie zu diesem Zweck jede bereits berechnete Fibonacci-Zahl in einer Tabelle."
                                        }>
                                        <Code noIframe={true} language={"html"}>
                                            {`<!doctype html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title>Fibonacci Folge berechnen</title>
    <script>
      var table = {
        1: 0,
        2: 1,
      };
      function fib(position) {
        if (position < 1) {
          throw new Error("Die Fibonacci folge ist nur für echt positive positionen definiert.");
        }
        if (table[position] !== undefined) {
          return table[position];
        } else {
          // @ts-ignore
          var value = fib(position - 1) + fib(position - 2);
          table[position] = value;
          return value;
        }
      }
    </script>
</head>

<body>
<h1>Fibonacci Folge berechnen</h1>
</body>
</html>`}
                                        </Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Was ist die größte Fibonacci-Zahl, die sich noch als Integer sicher speichern lässt (Number.MAX_SAFE_INTEGER)? Die wievielte Fibonacci-Zahl in der Fibonacci-Folge ist das?"
                                        }>
                                        <p>
                                            4.501513169589839e+306 <br />
                                            Dies ist die: 1470igste Zahl der Fibonacci Folge.
                                        </p>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Was ist die größte Fibonacci-Zahl, die sich noch als Number speichern lässt (Number.MAX_VALUE)? Die wievielte Fibonacci-Zahl in der Fibonacci-Folge ist das (d.h. welche Stelle in der Fibonacci-Folge)?"
                                        }>
                                        <p>
                                            1.3069892237633987e+308
                                            <br />
                                            Dies ist die 1477igste Zahl der Fibonacci Folge.
                                        </p>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Wechseln Sie zu BigInt, um alle 2000 Fibonacci-Zahlen korrekt anzuzeigen. Geben Sie hier HTML- und JavaScript-Code zusammen ein:"
                                        }>
                                        <Code noIframe={true} language={"html"}>
                                            {`<!doctype html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title>Fibonacci Folge berechnen</title>
    <script>
      var table = {
        1: 0n,
        2: 1n,
      };
      function fib(position) {
        if (position < 1) {
          throw new Error("Die Fibonacci folge ist nur für echt positive positionen definiert.");
        }
        if (table[position] !== undefined) {
          return table[position];
        } else {
          var value = fib(position - 1) + fib(position - 2);
          table[position] = value;
          return value;
        }
      }
    </script>
</head>

<body>
<h1>Fibonacci Folge berechnen</h1>
</body>
</html>

`}
                                        </Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                        <div>
                            <ExerciseGroup task={"Topsort"} taskNumber={"4.4"}>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            'In jedem Projekt fallen Aufgaben (Tasks) an. Zwischen den Aufgaben gibt es paarweise Abhängigkeiten. Z.B. kann Task2 von Task1 abhängen, d.h. erst muss Task1 fertig sein, bevor Task2 starten kann, weil es dessen Ergebnisse benötigt. Tasks können auch unabhängig voneinander sein und parallel ablaufen. In JavaScript können Sie die Abhängigkeiten in Arrays codieren, z.B. kann man bei [ ["schlafen", "studieren"], ["essen", "studieren"], ["studieren", "prüfen"] ] nicht prüfen, ohne vorher gegessen zu haben. Transitive Abhängigkeiten müssen also berücksichtigt werden.\n' +
                                            "\n" +
                                            "Schreiben Sie in JavaScript eine Funktion topsort(), die eine topologische Sortierung berechnet.\n" +
                                            "\n" +
                                            "Achten Sie auf Performanz. Berechnen Sie die topologische Sortierung in linearer Zeit (Average Case)."
                                        }>
                                        <Code noIframe={true} language={"html"}>
                                            {`<!doctype html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title>Topologische Sortierung</title>
</head>

<body>
<h1>Topologische Sortierung</h1>
<script>


    function topsort(arr) {
        function hasNoDependency(el, arr) {
            return !arr.some(deps => deps.length > 1 && deps[1] === el);

        }
        var sort = []
        while(arr.length > 0) {
            arr.forEach((deps) => {
                if (hasNoDependency(deps[0], arr)) {
                    sort.push(deps[0])
                    // remove it from arr.
                    arr = arr.map(d => d.filter(task => task !== deps[0]));
                    arr = arr.filter(d => d.length > 0);
                }
            })
        }
        return sort;
    }
    console.log(topsort([ ["essen", "studieren"], ["schlafen", "studieren"], ["studieren", "prüfen"] ]));
</script>
</body>
</html>`}
                                        </Code>
                                    </Exercise>
                                </div>
                                <div className={"mb-4"}>
                                    <Exercise
                                        task={
                                            "Testen Sie Ihren JavaScript-Code. Verwenden Sie für Ihre Tests die Funktion console.assert. Geben Sie hier Ihre Tests ein:"
                                        }>
                                        <Code noIframe={true} language={"html"}>{`<!doctype html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <title>Topologische Sortierung</title>
  </head>
  
  <body>
    <h1>Topologische Sortierung</h1>
    <script>
      function topsort(arr) {
        function hasNoDependency(el, arr) {
          return !arr.some(deps => deps.length > 1 && deps[1] === el);
        }
      
        var sort = []
        while(arr.length > 0) {
          arr.forEach((deps) => {
            if (hasNoDependency(deps[0], arr)) {
              sort.push(deps[0])
              // remove it from arr.
              arr = arr.map(d => d.filter(task => task !== deps[0]));
              arr = arr.filter(d => d.length > 0);
            }
          })
        }
        return sort;
      }
      console.log(topsort([ ["essen", "studieren"], ["schlafen", "studieren"], ["studieren", "prüfen"] ]));
      
      var result = topsort([ ["essen", "studieren"], ["schlafen", "studieren"], ["studieren", "prüfen"] ])
      console.assert(result.indexOf("essen") < result.indexOf("studieren"))
      console.assert(result.indexOf("essen") < result.indexOf("prüfen"))
      console.assert(result.indexOf("studieren") < result.indexOf("prüfen"))
    </script>
  </body>
</html>`}</Code>
                                    </Exercise>
                                </div>
                            </ExerciseGroup>
                        </div>
                    </div>
                </div>
            </div>
        </Page>
    );
};

export default U4;
