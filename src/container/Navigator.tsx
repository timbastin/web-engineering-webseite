import React, { ReactNode } from "react";
import { HashRouter as Router, Switch, Route } from "react-router-dom";
import routes, { AppRoute } from "../pages";
import { flattenDeep } from "lodash";

const renderRoute = (path: string, route: AppRoute): ReactNode => {
    const p = `${path}/${route.path}`;

    if ("children" in route) {
        return flattenDeep(route.children.map((appRoute) => renderRoute(p, appRoute)));
    }
    return [
        <Route key={path} exact={true} path={p}>
            {route.Component()}
        </Route>,
    ];
};

const Navigator = () => {
    return (
        <div id={"page"}>
            <Router>
                <Switch>{flattenDeep(routes.map((appRoute) => renderRoute("", appRoute)))}</Switch>
            </Router>
        </div>
    );
};

export default Navigator;
