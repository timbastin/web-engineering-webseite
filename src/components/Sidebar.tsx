import React, { FunctionComponent } from "react";
import AnchorLink, { AnchorLinkProps } from "./AnchorLink";

interface Props {
    links: AnchorLinkProps[];
}

const Sidebar: FunctionComponent<Props> = (props) => {
    return (
        <div className={"sidebar p-3"}>
            <h3>Navigation</h3>
            {props.links.map((link, index) => (
                <div className={"mt-3"} key={link.anchor}>
                    <AnchorLink {...link} title={index + 1 + ". " + link.title} />
                </div>
            ))}
        </div>
    );
};

export default Sidebar;
