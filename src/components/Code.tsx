import React, { FunctionComponent, useState } from "react";
import SyntaxHighlighter from "react-syntax-highlighter";
import theme from "react-syntax-highlighter/dist/esm/styles/hljs/a11y-light";
import { CodeOutlined } from "@ant-design/icons";

interface Props {
    language: string;
    noIframe?: boolean;
}
const Code: FunctionComponent<Props> = (props) => {
    const [codeVisible, setCodeVisible] = useState(props.noIframe === true);
    return (
        <>
            {!props.noIframe && (
                <>
                    {/* eslint-disable-next-line jsx-a11y/iframe-has-title */}
                    <iframe src={"data:text/html," + encodeURIComponent(props.children as string)} />
                    <div className={"show-code px-1 py-1 d-flex justify-content-end"}>
                        <button onClick={() => setCodeVisible((prevState) => !prevState)}>
                            <CodeOutlined className={"mr-2"} />
                            Code {codeVisible ? "verstecken" : "anzeigen"}
                        </button>
                    </div>
                </>
            )}
            <div className={"code-slide " + codeVisible}>
                <SyntaxHighlighter language={props.language} style={theme}>
                    {props.children}
                </SyntaxHighlighter>
            </div>
        </>
    );
};

export default Code;
