import React, { FunctionComponent } from "react";
import Header from "./Header";
import Footer from "./Footer";

interface Props {
    title: string;
}
const Page: FunctionComponent<Props> = (props) => {
    return (
        <>
            <Header title={props.title} />
            <main className={"pt-md-5 pt-3"}>{props.children}</main>
            <Footer />
        </>
    );
};

export default Page;
