import React, { FunctionComponent, MouseEventHandler } from "react";

export interface AnchorLinkProps {
    title: string;
    anchor: string;
}

const AnchorLink: FunctionComponent<AnchorLinkProps> = (props) => {
    const handleClick: MouseEventHandler = (e) => {
        e.preventDefault();
        const el = document.getElementById(props.anchor);
        if (el) {
            el.scrollIntoView({ behavior: "smooth" });
        }
    };
    return (
        <a onClick={handleClick} href={props.anchor}>
            {props.title}
        </a>
    );
};

export default AnchorLink;
