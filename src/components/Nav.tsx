import React, { FunctionComponent, useState } from "react";
import { Menu } from "antd";
import { useLocation, useHistory } from "react-router-dom";
import routes, { AppRoute } from "../pages";
import { MenuClickEventHandler, MenuInfo } from "rc-menu/lib/interface";

const { SubMenu } = Menu;
/**
 * Renders all submenu items recursively.
 * @param {string} path
 * @param {AppRoute} route
 * @returns {JSX.Element}
 */
const renderMenu = (route: AppRoute) => {
    if ("children" in route) {
        return (
            <SubMenu title={route.title} key={route.path}>
                {route.children.map((appRoute) => renderMenu(appRoute))}
            </SubMenu>
        );
    }
    return <Menu.Item key={route.path}>{route.title}</Menu.Item>;
};

interface Props {
    isMobile: boolean;
}
const Nav: FunctionComponent<Props> = (props) => {
    const location = useLocation();
    const history = useHistory();
    const [selectedKeys, selectKeys] = useState(location.pathname.substr(1).split("/"));
    const handleNavigation: MenuClickEventHandler = (info: MenuInfo) => {
        if (info.key === "vue-app") {
            window.open("vue-app", "_blank");
            return;
        }
        if (info.key === "item_0") {
            history.push("/");
        } else {
            history.push("/" + info.keyPath.reverse().join("/"));
        }

        selectKeys(info.keyPath as string[]);
    };
    return (
        <nav>
            <Menu
                onClick={handleNavigation}
                selectedKeys={selectedKeys}
                mode={props.isMobile ? "inline" : "horizontal"}>
                {routes.map((route) => renderMenu(route))}
                <Menu.Item key={"vue-app"}>Vue-App</Menu.Item>
            </Menu>
        </nav>
    );
};

export default Nav;
