import React, { FunctionComponent } from "react";

interface Props {
    task: string;
    taskNumber: string;
}
const ExerciseGroup: FunctionComponent<Props> = (props) => {
    return (
        <div id={props.taskNumber} className={"exercise-group"}>
            <h2 className={"mb-4"}>
                <span>{props.taskNumber}</span>&nbsp;{props.task}
            </h2>
            <div className={"exercise-children"}>{props.children}</div>
        </div>
    );
};

export default ExerciseGroup;
