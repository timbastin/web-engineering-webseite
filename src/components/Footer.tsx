import React from "react";
import { Link } from "react-router-dom";
import { GitlabFilled } from "@ant-design/icons";
const Footer = () => {
    return (
        <footer className={"bg-white"}>
            <div className={"container-fluid"}>
                <div className={"row"}>
                    <div className={"col-md-12 d-flex justify-content-end py-3"}>
                        <a
                            className={"mr-4"}
                            target={"_blank"}
                            rel={"noreferrer"}
                            href={"https://gitlab.com/timbastin/web-engineering-webseite"}>
                            <GitlabFilled style={{ fontSize: 20 }} />
                        </a>
                        <Link to="/impressum">Impressum</Link>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default Footer;
