import React, { FunctionComponent } from "react";

interface Props {
    task: string;
}
const Exercise: FunctionComponent<Props> = (props) => {
    return (
        <div className={"exercise"}>
            <div className={"task p-3 bg-primary"}>
                <i>{props.task}</i>
            </div>
            <div className={"content  px-3 pb-3"}>
                <p className={"mb-0 mt-3"}>{props.children}</p>
            </div>
        </div>
    );
};

export default Exercise;
