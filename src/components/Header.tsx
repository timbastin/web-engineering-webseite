import React, { FunctionComponent, useState } from "react";
import { Drawer, PageHeader } from "antd";
import Nav from "./Nav";
import { MenuOutlined } from "@ant-design/icons";

interface Props {
    title: string;
}
const Header: FunctionComponent<Props> = (props) => {
    const [isOpen, setIsOpen] = useState(false);
    return (
        <>
            <header className={"d-flex"}>
                <img
                    className={"ml-5"}
                    width={50}
                    alt={"hbrs-logo"}
                    src={
                        "data:image/svg+xml,%3C%3Fxml version='1.0' encoding='utf-8'%3F%3E%3C!-- Generator: Adobe Illustrator 25.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0) --%3E%3Csvg version='1.1' id='Ebene_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 504.4 212.5' style='enable-background:new 0 0 504.4 212.5;' xml:space='preserve'%3E%3Cstyle type='text/css'%3E .st0%7Bfill:%23009EE0;stroke:%23009EE0;stroke-width:2;%7D .st1%7Bfill:%23009EE0;%7D .st2%7Bfill:none;%7D%0A%3C/style%3E%3Cg id='Kreise_x5F_cyan'%3E%3Ccircle class='st0' cx='398.2' cy='106.3' r='105.3'/%3E%3C/g%3E%3Cg id='Kreis_innen_weis'%3E%3Cpath class='st1' d='M105.3,1C47.1,1,0,48.1,0,106.3c0,58.1,47.1,105.3,105.3,105.3c58.1,0,105.3-47.1,105.3-105.3 C210.5,48.1,163.4,1,105.3,1z M105.6,167.6c-34.3,0-62.1-27.8-62.1-62.1c0-34.3,27.8-62.1,62.1-62.1s62.1,27.8,62.1,62.1 S139.9,167.6,105.6,167.6z'/%3E%3Ccircle class='st2' cx='105.6' cy='105.5' r='62.1'/%3E%3C/g%3E%3C/svg%3E%0A"
                    }
                />

                <PageHeader title={"Web-Engineering"} subTitle={props.title} />
                <div className={"mobile-nav"}>
                    <MenuOutlined onClick={() => setIsOpen(true)} />
                    <Drawer visible={isOpen} onClose={() => setIsOpen(false)}>
                        <Nav isMobile={true} />
                    </Drawer>
                </div>
            </header>
            <div className={"desktop-nav nav"}>
                <div className={"container"}>
                    <Nav isMobile={false} />
                </div>
            </div>
        </>
    );
};

export default Header;
