import "./scss/style.scss";
import "./less/antd.less";
require("file-loader?name=[name].[ext]!../index.html");
import "react-hot-loader";
import "./bootstrap.tsx";

// @ts-ignore
if (module.hot) {
    // @ts-ignore
    module.hot.accept();
}
