import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

const container = document.getElementById("root");
if (container) {
    ReactDOM.render(<App />, container);
} else {
    throw new Error("root container not found");
}
