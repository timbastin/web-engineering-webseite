var path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
    mode: "development",
    entry: "./src/index.ts",
    output: {
        filename: "bundle.js",
        path: `${__dirname}/dist`,
        publicPath: "/static/",
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json"],
    },
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        compress: true,
        hot: true,
        port: 9000,
    },
    devtool: "source-map",

    plugins: [
        new CopyWebpackPlugin({
            patterns: [
                // relative path is from src
                { from: "./static/" },
                // copy the vue-js app to the dist directory.
                // we depend on the installation and building of it.
                // good that we have a ci/cd
                { from: "vue-app/dist", to: "vue-app" },
                { from: "php", to: "php" },
            ],
        }),
    ],
    module: {
        rules: [
            {
                test: /\.scss$/,
                include: path.resolve(__dirname, "src"),
                use: ["style-loader", "css-loader", "sass-loader"],
            },
            {
                test: /\.less$/,
                include: path.resolve(__dirname, "src"),
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" },
                    { loader: "less-loader", options: { lessOptions: { javascriptEnabled: true } } },
                ],
            },
            { test: /\.tsx?$/, include: path.resolve(__dirname, "src"), loader: "babel-loader" },
            { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },
        ],
    },
};
