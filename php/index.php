<?php

require_once("./cors.php");

class UserService {
    private $data;
    function __construct() {
        $this->data = json_decode(file_get_contents("./user.json"), true);
    }
    public function check_if_registered($email) {
        return $this->get_user($email) !== null;
    }

    private function get_user($email) {
        foreach ($this->data as $e => $password) {
            if ($e === $email) {
                return [
                    "email" => $e,
                    "password" => $password,
                ];
            }
        }
        return null;
    }

    public function register_user($email, $password) {
        $this->data[$email] = password_hash($password, PASSWORD_BCRYPT);
        $this->updateFile();
    }

    private function updateFile() {
        file_put_contents("./user.json", json_encode($this->data));
    }

    public function verify($email, $password) {
        $user = $this->get_user($email);
        if (!$user) {
            return false;
        }

        return password_verify($password, $user["password"]);
    }
}

cors();
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE);
if (!$input["email"] || !$input["password"]) {
    http_response_code(400);
    return;
}

$controller = new UserService();

function register() {
    global $controller;
    global $input;
    // check if the user is already registered.
    if ($controller->check_if_registered($input["email"])) {
        http_response_code(409);
        return [
            "success" => false
        ];
    }

    // we can add the user.
    $controller->register_user($input["email"], $input["password"]);
    return [
        "success" => true
    ];
}

function login() {
    global $controller;
    global $input;

    if (!$controller->verify($input["email"], $input["password"])) {
        http_response_code(403);
        return [
            "success" => false
        ];
    }
    return [
        "success" => true
    ];
}


switch ($input["action"]) {
    case 'register':
        echo json_encode(register());
        return;
    case "login":
        echo json_encode(login());
}
