<?php

require_once("./cors.php");
cors();

$data = json_decode(file_get_contents("./www-navigator-content.json"), true);
switch($_SERVER["REQUEST_METHOD"]){
    case 'POST':
        // update the value
        $inputJSON = file_get_contents('php://input');
        $input = json_decode($inputJSON, TRUE);
        $data[$input["key"]][$input["category"]]["content"] = $input["content"];
        file_put_contents("./www-navigator-content.json", json_encode($data));
        echo json_encode([
            "success" => true,
        ]);
        break;
    case "GET":
        // retrieve the data.
        echo json_encode($data[$_GET['data']]);
}
