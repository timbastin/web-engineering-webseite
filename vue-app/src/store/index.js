import { createStore } from 'vuex'

export default createStore({
  state: {
    data: {}
  },
  mutations: {
    newData(state, payload) {
      state.data = payload;
    },
  },
  actions: {
    async fetchData({commit}, fileName) {
      // ...
      const response = await fetch("http://www2.inf.h-brs.de/~tbasti2s/" + fileName)
      commit("newData", await response.json())
    }
  },
})
