export default async function updateData(key, category, content) {
  const response = await fetch("http://www2.inf.h-brs.de/~tbasti2s/php/www-navigator.php", {
    method: "POST",
    body: JSON.stringify({
      key,
      category,
      content,
    })
  });
  return response.json()
}
