import { createRouter, createWebHashHistory } from 'vue-router'
import HTML from "@/views/HTML";
import CSS from "@/views/CSS";
import JavaScript from "@/views/JavaScript";

const routes = [
  {
    path: '/html',
    name: 'HTML',
    component: HTML,
  },
  {
    path: '/css',
    name: 'CSS',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: CSS
  },
  {
    path: "/javascript",
    name: "JavaScript",
    component: JavaScript
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
